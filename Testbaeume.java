import java.util.ArrayList;
import java.io.FileReader;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Liest Testbäume aus einer Datei aus
 * 
 * @author Rainer Helfrich
 * @version November 2020
 */
public class Testbaeume
{
    /**
     * Die aus der Datei eingelesenen Bäume
     */
    static ArrayList<Binaerbaum<Integer>> baeume;

    private static void readFile() 
    {
        try
        {
            // Hier kann man anstelle der Baeume in "gross.txt" auch erst 
            // mal übersichtlichere kleine Bäume aus der Datei "klein.txt"
            // einlesen. 
            // In gross.txt sind 100 Bäume mit den Indizes 0-99 definiert
            // In klein.txt sind 5 Bäume mit den Indizes 0-4 definiert
            FileReader fileReader = new FileReader("gross.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            baeume = new ArrayList<Binaerbaum<Integer>>();
            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
                AtomicInteger ai = new AtomicInteger(0);
                Binaerbaum<Integer> b = parse(line.toCharArray(), ai);
                baeume.add(b);
            }
            bufferedReader.close();
        }
        catch(Exception x)
        {
            System.out.println(x.getMessage());
            baeume = null;
        }
    }

    private static Binaerbaum<Integer> parse(char[] c, AtomicInteger s)
    {
        int anfang = s.incrementAndGet();
        while(c[s.get()] != '(' && c[s.get()] != ')')
        {
            s.incrementAndGet();
        }
        String tmp = new String(c, anfang, s.get()-anfang);
        int value = Integer.parseInt(tmp);
        Binaerbaum<Integer> ergebnis = new Binaerbaum<Integer>(value);
        if (c[s.get()] == '(')
        {
            ergebnis.links = parse(c, s);
            s.incrementAndGet();
            ergebnis.rechts = parse(c, s);
            s.incrementAndGet();
        }
        return ergebnis;
    }

    /**
     * Gibt einen der Testbäume aus der Datei zurück
     * @param nummer Die Nummer des Baums
     * @return Den gewünschten Baum, wenn die Nummer gültig ist; null sonst
     */
    public static Binaerbaum<Integer> getTestBaum(int nummer)
    {
        if (baeume == null)
            readFile();
        if (nummer >= 0 && nummer < baeume.size())
            return baeume.get(nummer);
        return null;
    }
}
