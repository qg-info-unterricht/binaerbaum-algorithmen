import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Eine Testklasse für Baumalgorithmen
 *
 * @author  Rainer Helfrich
 * @version 02.11.2020
 */
public class BaumTester
{
    @Test
    public void test001()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(0));
        assertEquals(481,ba.anzahl());
        assertEquals(12,ba.tiefe());
        assertTrue(ba.enthaelt(512));
        assertTrue(ba.enthaelt(596));
        assertFalse(ba.enthaelt(70));
        assertFalse(ba.enthaelt(1008));
        assertFalse(ba.enthaelt(510));
        assertFalse(ba.enthaelt(554));
        assertFalse(ba.enthaelt(287));
        assertFalse(ba.enthaelt(99));
        assertTrue(ba.enthaelt(598));
        assertTrue(ba.enthaelt(875));
        assertFalse(ba.enthaelt(949));
        assertFalse(ba.enthaelt(974));
        assertFalse(ba.enthaelt(917));
        assertFalse(ba.enthaelt(146));
        assertTrue(ba.enthaelt(366));
        assertTrue(ba.enthaelt(367));
        assertTrue(ba.enthaelt(411));
        assertTrue(ba.enthaelt(900));
        assertTrue(ba.enthaelt(430));
        assertTrue(ba.enthaelt(175));
    }

    @Test
    public void test002()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(1));
        assertEquals(183,ba.anzahl());
        assertEquals(9,ba.tiefe());
        assertFalse(ba.enthaelt(711));
        assertFalse(ba.enthaelt(835));
        assertFalse(ba.enthaelt(637));
        assertFalse(ba.enthaelt(888));
        assertFalse(ba.enthaelt(962));
        assertTrue(ba.enthaelt(692));
        assertTrue(ba.enthaelt(731));
        assertFalse(ba.enthaelt(2));
        assertFalse(ba.enthaelt(417));
        assertFalse(ba.enthaelt(667));
        assertFalse(ba.enthaelt(549));
        assertTrue(ba.enthaelt(277));
        assertTrue(ba.enthaelt(934));
        assertFalse(ba.enthaelt(641));
        assertTrue(ba.enthaelt(825));
        assertTrue(ba.enthaelt(592));
        assertTrue(ba.enthaelt(958));
        assertTrue(ba.enthaelt(416));
        assertTrue(ba.enthaelt(680));
        assertTrue(ba.enthaelt(388));
    }

    @Test
    public void test003()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(2));
        assertEquals(105,ba.anzahl());
        assertEquals(8,ba.tiefe());
        assertTrue(ba.enthaelt(985));
        assertFalse(ba.enthaelt(224));
        assertFalse(ba.enthaelt(374));
        assertFalse(ba.enthaelt(304));
        assertFalse(ba.enthaelt(814));
        assertFalse(ba.enthaelt(67));
        assertFalse(ba.enthaelt(972));
        assertFalse(ba.enthaelt(384));
        assertFalse(ba.enthaelt(845));
        assertFalse(ba.enthaelt(762));
        assertFalse(ba.enthaelt(353));
        assertTrue(ba.enthaelt(656));
        assertTrue(ba.enthaelt(207));
        assertTrue(ba.enthaelt(954));
        assertTrue(ba.enthaelt(423));
        assertTrue(ba.enthaelt(163));
        assertTrue(ba.enthaelt(968));
        assertTrue(ba.enthaelt(69));
        assertTrue(ba.enthaelt(589));
        assertTrue(ba.enthaelt(189));
    }

    @Test
    public void test004()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(3));
        assertEquals(203,ba.anzahl());
        assertEquals(10,ba.tiefe());
        assertFalse(ba.enthaelt(37));
        assertFalse(ba.enthaelt(132));
        assertFalse(ba.enthaelt(921));
        assertFalse(ba.enthaelt(936));
        assertFalse(ba.enthaelt(793));
        assertFalse(ba.enthaelt(296));
        assertFalse(ba.enthaelt(603));
        assertFalse(ba.enthaelt(395));
        assertFalse(ba.enthaelt(681));
        assertFalse(ba.enthaelt(141));
        assertTrue(ba.enthaelt(523));
        assertTrue(ba.enthaelt(305));
        assertTrue(ba.enthaelt(315));
        assertTrue(ba.enthaelt(849));
        assertTrue(ba.enthaelt(567));
        assertTrue(ba.enthaelt(666));
        assertTrue(ba.enthaelt(73));
        assertTrue(ba.enthaelt(76));
        assertTrue(ba.enthaelt(814));
        assertTrue(ba.enthaelt(585));
    }

    @Test
    public void test005()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(4));
        assertEquals(133,ba.anzahl());
        assertEquals(9,ba.tiefe());
        assertFalse(ba.enthaelt(207));
        assertFalse(ba.enthaelt(370));
        assertFalse(ba.enthaelt(13));
        assertFalse(ba.enthaelt(703));
        assertFalse(ba.enthaelt(161));
        assertFalse(ba.enthaelt(966));
        assertFalse(ba.enthaelt(565));
        assertFalse(ba.enthaelt(236));
        assertFalse(ba.enthaelt(468));
        assertTrue(ba.enthaelt(428));
        assertFalse(ba.enthaelt(312));
        assertTrue(ba.enthaelt(715));
        assertTrue(ba.enthaelt(205));
        assertTrue(ba.enthaelt(854));
        assertTrue(ba.enthaelt(657));
        assertTrue(ba.enthaelt(232));
        assertTrue(ba.enthaelt(659));
        assertTrue(ba.enthaelt(55));
        assertTrue(ba.enthaelt(598));
        assertTrue(ba.enthaelt(127));
    }

    @Test
    public void test006()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(5));
        assertEquals(159,ba.anzahl());
        assertEquals(9,ba.tiefe());
        assertFalse(ba.enthaelt(243));
        assertFalse(ba.enthaelt(248));
        assertFalse(ba.enthaelt(810));
        assertFalse(ba.enthaelt(206));
        assertFalse(ba.enthaelt(285));
        assertFalse(ba.enthaelt(743));
        assertFalse(ba.enthaelt(187));
        assertTrue(ba.enthaelt(4));
        assertFalse(ba.enthaelt(497));
        assertTrue(ba.enthaelt(983));
        assertFalse(ba.enthaelt(1007));
        assertFalse(ba.enthaelt(47));
        assertTrue(ba.enthaelt(83));
        assertTrue(ba.enthaelt(116));
        assertTrue(ba.enthaelt(287));
        assertTrue(ba.enthaelt(423));
        assertTrue(ba.enthaelt(165));
        assertTrue(ba.enthaelt(905));
        assertTrue(ba.enthaelt(760));
        assertTrue(ba.enthaelt(985));
    }

    @Test
    public void test007()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(6));
        assertEquals(165,ba.anzahl());
        assertEquals(10,ba.tiefe());
        assertFalse(ba.enthaelt(467));
        assertFalse(ba.enthaelt(452));
        assertTrue(ba.enthaelt(737));
        assertFalse(ba.enthaelt(849));
        assertFalse(ba.enthaelt(213));
        assertFalse(ba.enthaelt(610));
        assertFalse(ba.enthaelt(73));
        assertTrue(ba.enthaelt(472));
        assertFalse(ba.enthaelt(603));
        assertFalse(ba.enthaelt(739));
        assertFalse(ba.enthaelt(193));
        assertFalse(ba.enthaelt(704));
        assertTrue(ba.enthaelt(828));
        assertTrue(ba.enthaelt(178));
        assertTrue(ba.enthaelt(702));
        assertTrue(ba.enthaelt(341));
        assertTrue(ba.enthaelt(78));
        assertTrue(ba.enthaelt(640));
        assertTrue(ba.enthaelt(916));
        assertTrue(ba.enthaelt(227));
    }

    @Test
    public void test008()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(7));
        assertEquals(389,ba.anzahl());
        assertEquals(12,ba.tiefe());
        assertTrue(ba.enthaelt(772));
        assertFalse(ba.enthaelt(426));
        assertFalse(ba.enthaelt(953));
        assertTrue(ba.enthaelt(319));
        assertFalse(ba.enthaelt(467));
        assertFalse(ba.enthaelt(417));
        assertTrue(ba.enthaelt(883));
        assertTrue(ba.enthaelt(657));
        assertFalse(ba.enthaelt(1006));
        assertFalse(ba.enthaelt(30));
        assertFalse(ba.enthaelt(80));
        assertFalse(ba.enthaelt(183));
        assertFalse(ba.enthaelt(318));
        assertTrue(ba.enthaelt(219));
        assertTrue(ba.enthaelt(438));
        assertTrue(ba.enthaelt(412));
        assertFalse(ba.enthaelt(145));
        assertTrue(ba.enthaelt(326));
        assertTrue(ba.enthaelt(572));
        assertTrue(ba.enthaelt(208));
    }

    @Test
    public void test009()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(8));
        assertEquals(159,ba.anzahl());
        assertEquals(11,ba.tiefe());
        assertFalse(ba.enthaelt(746));
        assertFalse(ba.enthaelt(288));
        assertFalse(ba.enthaelt(752));
        assertFalse(ba.enthaelt(651));
        assertFalse(ba.enthaelt(850));
        assertFalse(ba.enthaelt(267));
        assertTrue(ba.enthaelt(197));
        assertFalse(ba.enthaelt(264));
        assertFalse(ba.enthaelt(1009));
        assertFalse(ba.enthaelt(19));
        assertFalse(ba.enthaelt(166));
        assertTrue(ba.enthaelt(182));
        assertTrue(ba.enthaelt(623));
        assertTrue(ba.enthaelt(25));
        assertTrue(ba.enthaelt(900));
        assertTrue(ba.enthaelt(709));
        assertTrue(ba.enthaelt(658));
        assertTrue(ba.enthaelt(853));
        assertTrue(ba.enthaelt(628));
        assertTrue(ba.enthaelt(202));
    }

    @Test
    public void test010()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(9));
        assertEquals(137,ba.anzahl());
        assertEquals(9,ba.tiefe());
        assertFalse(ba.enthaelt(856));
        assertFalse(ba.enthaelt(402));
        assertFalse(ba.enthaelt(165));
        assertFalse(ba.enthaelt(993));
        assertTrue(ba.enthaelt(904));
        assertFalse(ba.enthaelt(211));
        assertTrue(ba.enthaelt(845));
        assertFalse(ba.enthaelt(259));
        assertFalse(ba.enthaelt(544));
        assertFalse(ba.enthaelt(498));
        assertFalse(ba.enthaelt(852));
        assertFalse(ba.enthaelt(65));
        assertTrue(ba.enthaelt(805));
        assertTrue(ba.enthaelt(224));
        assertTrue(ba.enthaelt(213));
        assertTrue(ba.enthaelt(604));
        assertTrue(ba.enthaelt(273));
        assertTrue(ba.enthaelt(161));
        assertTrue(ba.enthaelt(437));
        assertTrue(ba.enthaelt(590));
    }

    @Test
    public void test011()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(10));
        assertEquals(313,ba.anzahl());
        assertEquals(11,ba.tiefe());
        assertFalse(ba.enthaelt(895));
        assertTrue(ba.enthaelt(344));
        assertFalse(ba.enthaelt(966));
        assertFalse(ba.enthaelt(258));
        assertFalse(ba.enthaelt(1002));
        assertFalse(ba.enthaelt(181));
        assertFalse(ba.enthaelt(661));
        assertFalse(ba.enthaelt(217));
        assertFalse(ba.enthaelt(54));
        assertTrue(ba.enthaelt(36));
        assertTrue(ba.enthaelt(690));
        assertTrue(ba.enthaelt(703));
        assertTrue(ba.enthaelt(185));
        assertFalse(ba.enthaelt(657));
        assertTrue(ba.enthaelt(934));
        assertFalse(ba.enthaelt(854));
        assertTrue(ba.enthaelt(49));
        assertTrue(ba.enthaelt(907));
        assertTrue(ba.enthaelt(861));
        assertTrue(ba.enthaelt(348));
    }

    @Test
    public void test012()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(11));
        assertEquals(313,ba.anzahl());
        assertEquals(11,ba.tiefe());
        assertFalse(ba.enthaelt(42));
        assertTrue(ba.enthaelt(282));
        assertFalse(ba.enthaelt(733));
        assertTrue(ba.enthaelt(650));
        assertTrue(ba.enthaelt(61));
        assertFalse(ba.enthaelt(780));
        assertFalse(ba.enthaelt(1013));
        assertFalse(ba.enthaelt(368));
        assertFalse(ba.enthaelt(703));
        assertFalse(ba.enthaelt(362));
        assertFalse(ba.enthaelt(559));
        assertFalse(ba.enthaelt(1001));
        assertFalse(ba.enthaelt(987));
        assertTrue(ba.enthaelt(582));
        assertTrue(ba.enthaelt(845));
        assertTrue(ba.enthaelt(538));
        assertTrue(ba.enthaelt(477));
        assertTrue(ba.enthaelt(176));
        assertTrue(ba.enthaelt(237));
        assertTrue(ba.enthaelt(656));
    }

    @Test
    public void test013()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(12));
        assertEquals(171,ba.anzahl());
        assertEquals(9,ba.tiefe());
        assertFalse(ba.enthaelt(367));
        assertFalse(ba.enthaelt(6));
        assertFalse(ba.enthaelt(146));
        assertFalse(ba.enthaelt(896));
        assertFalse(ba.enthaelt(114));
        assertFalse(ba.enthaelt(542));
        assertFalse(ba.enthaelt(447));
        assertTrue(ba.enthaelt(51));
        assertFalse(ba.enthaelt(399));
        assertFalse(ba.enthaelt(43));
        assertTrue(ba.enthaelt(595));
        assertTrue(ba.enthaelt(831));
        assertFalse(ba.enthaelt(259));
        assertTrue(ba.enthaelt(960));
        assertTrue(ba.enthaelt(919));
        assertTrue(ba.enthaelt(755));
        assertTrue(ba.enthaelt(394));
        assertTrue(ba.enthaelt(466));
        assertTrue(ba.enthaelt(459));
        assertTrue(ba.enthaelt(539));
    }

    @Test
    public void test014()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(13));
        assertEquals(135,ba.anzahl());
        assertEquals(9,ba.tiefe());
        assertFalse(ba.enthaelt(898));
        assertFalse(ba.enthaelt(204));
        assertFalse(ba.enthaelt(870));
        assertFalse(ba.enthaelt(978));
        assertFalse(ba.enthaelt(871));
        assertFalse(ba.enthaelt(669));
        assertFalse(ba.enthaelt(793));
        assertFalse(ba.enthaelt(795));
        assertFalse(ba.enthaelt(480));
        assertTrue(ba.enthaelt(449));
        assertFalse(ba.enthaelt(380));
        assertTrue(ba.enthaelt(328));
        assertTrue(ba.enthaelt(436));
        assertTrue(ba.enthaelt(120));
        assertTrue(ba.enthaelt(433));
        assertTrue(ba.enthaelt(999));
        assertTrue(ba.enthaelt(879));
        assertTrue(ba.enthaelt(802));
        assertTrue(ba.enthaelt(517));
        assertTrue(ba.enthaelt(993));
    }

    @Test
    public void test015()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(14));
        assertEquals(101,ba.anzahl());
        assertEquals(8,ba.tiefe());
        assertFalse(ba.enthaelt(583));
        assertFalse(ba.enthaelt(693));
        assertFalse(ba.enthaelt(229));
        assertFalse(ba.enthaelt(157));
        assertFalse(ba.enthaelt(170));
        assertFalse(ba.enthaelt(651));
        assertFalse(ba.enthaelt(749));
        assertFalse(ba.enthaelt(996));
        assertFalse(ba.enthaelt(489));
        assertFalse(ba.enthaelt(116));
        assertTrue(ba.enthaelt(458));
        assertTrue(ba.enthaelt(99));
        assertTrue(ba.enthaelt(687));
        assertTrue(ba.enthaelt(727));
        assertTrue(ba.enthaelt(329));
        assertTrue(ba.enthaelt(420));
        assertTrue(ba.enthaelt(954));
        assertTrue(ba.enthaelt(155));
        assertTrue(ba.enthaelt(973));
        assertTrue(ba.enthaelt(103));
    }

    @Test
    public void test016()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(15));
        assertEquals(105,ba.anzahl());
        assertEquals(8,ba.tiefe());
        assertFalse(ba.enthaelt(796));
        assertFalse(ba.enthaelt(842));
        assertFalse(ba.enthaelt(853));
        assertFalse(ba.enthaelt(217));
        assertFalse(ba.enthaelt(795));
        assertFalse(ba.enthaelt(736));
        assertFalse(ba.enthaelt(76));
        assertFalse(ba.enthaelt(325));
        assertFalse(ba.enthaelt(258));
        assertFalse(ba.enthaelt(483));
        assertTrue(ba.enthaelt(190));
        assertTrue(ba.enthaelt(590));
        assertTrue(ba.enthaelt(16));
        assertTrue(ba.enthaelt(534));
        assertTrue(ba.enthaelt(762));
        assertTrue(ba.enthaelt(567));
        assertTrue(ba.enthaelt(169));
        assertTrue(ba.enthaelt(775));
        assertTrue(ba.enthaelt(85));
        assertTrue(ba.enthaelt(760));
    }

    @Test
    public void test017()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(16));
        assertEquals(95,ba.anzahl());
        assertEquals(9,ba.tiefe());
        assertFalse(ba.enthaelt(745));
        assertTrue(ba.enthaelt(293));
        assertFalse(ba.enthaelt(572));
        assertFalse(ba.enthaelt(19));
        assertFalse(ba.enthaelt(472));
        assertFalse(ba.enthaelt(180));
        assertFalse(ba.enthaelt(188));
        assertTrue(ba.enthaelt(902));
        assertFalse(ba.enthaelt(944));
        assertFalse(ba.enthaelt(118));
        assertFalse(ba.enthaelt(356));
        assertFalse(ba.enthaelt(973));
        assertTrue(ba.enthaelt(316));
        assertTrue(ba.enthaelt(53));
        assertTrue(ba.enthaelt(54));
        assertTrue(ba.enthaelt(807));
        assertTrue(ba.enthaelt(520));
        assertTrue(ba.enthaelt(429));
        assertTrue(ba.enthaelt(220));
        assertTrue(ba.enthaelt(194));
    }

    @Test
    public void test018()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(17));
        assertEquals(315,ba.anzahl());
        assertEquals(12,ba.tiefe());
        assertFalse(ba.enthaelt(646));
        assertFalse(ba.enthaelt(837));
        assertTrue(ba.enthaelt(543));
        assertFalse(ba.enthaelt(696));
        assertFalse(ba.enthaelt(307));
        assertTrue(ba.enthaelt(140));
        assertFalse(ba.enthaelt(835));
        assertFalse(ba.enthaelt(169));
        assertFalse(ba.enthaelt(498));
        assertFalse(ba.enthaelt(729));
        assertFalse(ba.enthaelt(405));
        assertFalse(ba.enthaelt(178));
        assertTrue(ba.enthaelt(97));
        assertTrue(ba.enthaelt(764));
        assertTrue(ba.enthaelt(743));
        assertTrue(ba.enthaelt(418));
        assertTrue(ba.enthaelt(954));
        assertTrue(ba.enthaelt(381));
        assertTrue(ba.enthaelt(997));
        assertTrue(ba.enthaelt(534));
    }

    @Test
    public void test019()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(18));
        assertEquals(301,ba.anzahl());
        assertEquals(12,ba.tiefe());
        assertFalse(ba.enthaelt(709));
        assertFalse(ba.enthaelt(22));
        assertFalse(ba.enthaelt(454));
        assertFalse(ba.enthaelt(64));
        assertFalse(ba.enthaelt(892));
        assertFalse(ba.enthaelt(176));
        assertFalse(ba.enthaelt(680));
        assertFalse(ba.enthaelt(729));
        assertFalse(ba.enthaelt(103));
        assertFalse(ba.enthaelt(198));
        assertTrue(ba.enthaelt(618));
        assertTrue(ba.enthaelt(877));
        assertTrue(ba.enthaelt(917));
        assertTrue(ba.enthaelt(810));
        assertTrue(ba.enthaelt(640));
        assertTrue(ba.enthaelt(98));
        assertTrue(ba.enthaelt(521));
        assertTrue(ba.enthaelt(586));
        assertTrue(ba.enthaelt(525));
        assertTrue(ba.enthaelt(197));
    }

    @Test
    public void test020()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(19));
        assertEquals(115,ba.anzahl());
        assertEquals(8,ba.tiefe());
        assertFalse(ba.enthaelt(719));
        assertFalse(ba.enthaelt(375));
        assertFalse(ba.enthaelt(471));
        assertFalse(ba.enthaelt(742));
        assertFalse(ba.enthaelt(362));
        assertFalse(ba.enthaelt(402));
        assertFalse(ba.enthaelt(682));
        assertFalse(ba.enthaelt(336));
        assertFalse(ba.enthaelt(759));
        assertFalse(ba.enthaelt(677));
        assertTrue(ba.enthaelt(75));
        assertTrue(ba.enthaelt(895));
        assertTrue(ba.enthaelt(669));
        assertTrue(ba.enthaelt(495));
        assertTrue(ba.enthaelt(925));
        assertTrue(ba.enthaelt(322));
        assertTrue(ba.enthaelt(600));
        assertTrue(ba.enthaelt(875));
        assertTrue(ba.enthaelt(807));
        assertTrue(ba.enthaelt(853));
    }

    @Test
    public void test021()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(20));
        assertEquals(279,ba.anzahl());
        assertEquals(11,ba.tiefe());
        assertTrue(ba.enthaelt(439));
        assertTrue(ba.enthaelt(687));
        assertFalse(ba.enthaelt(216));
        assertFalse(ba.enthaelt(621));
        assertFalse(ba.enthaelt(874));
        assertFalse(ba.enthaelt(516));
        assertTrue(ba.enthaelt(335));
        assertFalse(ba.enthaelt(703));
        assertTrue(ba.enthaelt(132));
        assertTrue(ba.enthaelt(630));
        assertFalse(ba.enthaelt(679));
        assertFalse(ba.enthaelt(269));
        assertFalse(ba.enthaelt(606));
        assertTrue(ba.enthaelt(212));
        assertTrue(ba.enthaelt(422));
        assertFalse(ba.enthaelt(43));
        assertFalse(ba.enthaelt(272));
        assertTrue(ba.enthaelt(410));
        assertTrue(ba.enthaelt(235));
        assertTrue(ba.enthaelt(103));
    }

    @Test
    public void test022()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(21));
        assertEquals(215,ba.anzahl());
        assertEquals(9,ba.tiefe());
        assertTrue(ba.enthaelt(310));
        assertFalse(ba.enthaelt(292));
        assertFalse(ba.enthaelt(474));
        assertFalse(ba.enthaelt(123));
        assertTrue(ba.enthaelt(824));
        assertFalse(ba.enthaelt(756));
        assertFalse(ba.enthaelt(829));
        assertFalse(ba.enthaelt(201));
        assertFalse(ba.enthaelt(755));
        assertFalse(ba.enthaelt(393));
        assertFalse(ba.enthaelt(1015));
        assertFalse(ba.enthaelt(3));
        assertTrue(ba.enthaelt(19));
        assertTrue(ba.enthaelt(343));
        assertTrue(ba.enthaelt(715));
        assertTrue(ba.enthaelt(662));
        assertTrue(ba.enthaelt(73));
        assertTrue(ba.enthaelt(195));
        assertTrue(ba.enthaelt(365));
        assertTrue(ba.enthaelt(889));
    }

    @Test
    public void test023()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(22));
        assertEquals(107,ba.anzahl());
        assertEquals(9,ba.tiefe());
        assertFalse(ba.enthaelt(821));
        assertFalse(ba.enthaelt(165));
        assertFalse(ba.enthaelt(884));
        assertFalse(ba.enthaelt(133));
        assertFalse(ba.enthaelt(545));
        assertFalse(ba.enthaelt(188));
        assertFalse(ba.enthaelt(435));
        assertFalse(ba.enthaelt(493));
        assertFalse(ba.enthaelt(823));
        assertFalse(ba.enthaelt(355));
        assertTrue(ba.enthaelt(276));
        assertTrue(ba.enthaelt(84));
        assertTrue(ba.enthaelt(265));
        assertTrue(ba.enthaelt(140));
        assertTrue(ba.enthaelt(529));
        assertTrue(ba.enthaelt(877));
        assertTrue(ba.enthaelt(767));
        assertTrue(ba.enthaelt(879));
        assertTrue(ba.enthaelt(724));
        assertTrue(ba.enthaelt(352));
    }

    @Test
    public void test024()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(23));
        assertEquals(207,ba.anzahl());
        assertEquals(11,ba.tiefe());
        assertFalse(ba.enthaelt(402));
        assertFalse(ba.enthaelt(268));
        assertFalse(ba.enthaelt(332));
        assertFalse(ba.enthaelt(818));
        assertFalse(ba.enthaelt(666));
        assertTrue(ba.enthaelt(36));
        assertFalse(ba.enthaelt(172));
        assertFalse(ba.enthaelt(537));
        assertFalse(ba.enthaelt(1015));
        assertFalse(ba.enthaelt(944));
        assertFalse(ba.enthaelt(941));
        assertTrue(ba.enthaelt(665));
        assertTrue(ba.enthaelt(967));
        assertTrue(ba.enthaelt(667));
        assertTrue(ba.enthaelt(684));
        assertTrue(ba.enthaelt(125));
        assertTrue(ba.enthaelt(29));
        assertTrue(ba.enthaelt(597));
        assertTrue(ba.enthaelt(446));
        assertTrue(ba.enthaelt(800));
    }

    @Test
    public void test025()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(24));
        assertEquals(119,ba.anzahl());
        assertEquals(9,ba.tiefe());
        assertFalse(ba.enthaelt(694));
        assertFalse(ba.enthaelt(872));
        assertFalse(ba.enthaelt(197));
        assertFalse(ba.enthaelt(983));
        assertFalse(ba.enthaelt(470));
        assertFalse(ba.enthaelt(487));
        assertTrue(ba.enthaelt(44));
        assertFalse(ba.enthaelt(552));
        assertFalse(ba.enthaelt(961));
        assertFalse(ba.enthaelt(714));
        assertFalse(ba.enthaelt(623));
        assertTrue(ba.enthaelt(18));
        assertTrue(ba.enthaelt(444));
        assertTrue(ba.enthaelt(277));
        assertTrue(ba.enthaelt(243));
        assertTrue(ba.enthaelt(852));
        assertTrue(ba.enthaelt(864));
        assertTrue(ba.enthaelt(765));
        assertTrue(ba.enthaelt(999));
        assertTrue(ba.enthaelt(488));
    }

    @Test
    public void test026()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(25));
        assertEquals(247,ba.anzahl());
        assertEquals(12,ba.tiefe());
        assertFalse(ba.enthaelt(782));
        assertFalse(ba.enthaelt(999));
        assertTrue(ba.enthaelt(173));
        assertFalse(ba.enthaelt(261));
        assertFalse(ba.enthaelt(892));
        assertTrue(ba.enthaelt(271));
        assertFalse(ba.enthaelt(420));
        assertTrue(ba.enthaelt(263));
        assertTrue(ba.enthaelt(919));
        assertFalse(ba.enthaelt(70));
        assertFalse(ba.enthaelt(4));
        assertFalse(ba.enthaelt(462));
        assertFalse(ba.enthaelt(877));
        assertFalse(ba.enthaelt(568));
        assertTrue(ba.enthaelt(834));
        assertTrue(ba.enthaelt(382));
        assertTrue(ba.enthaelt(861));
        assertTrue(ba.enthaelt(575));
        assertTrue(ba.enthaelt(19));
        assertTrue(ba.enthaelt(528));
    }

    @Test
    public void test027()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(26));
        assertEquals(179,ba.anzahl());
        assertEquals(9,ba.tiefe());
        assertTrue(ba.enthaelt(626));
        assertFalse(ba.enthaelt(340));
        assertFalse(ba.enthaelt(365));
        assertFalse(ba.enthaelt(954));
        assertFalse(ba.enthaelt(366));
        assertFalse(ba.enthaelt(814));
        assertFalse(ba.enthaelt(900));
        assertTrue(ba.enthaelt(496));
        assertFalse(ba.enthaelt(133));
        assertFalse(ba.enthaelt(107));
        assertTrue(ba.enthaelt(391));
        assertFalse(ba.enthaelt(801));
        assertFalse(ba.enthaelt(420));
        assertTrue(ba.enthaelt(310));
        assertTrue(ba.enthaelt(277));
        assertTrue(ba.enthaelt(262));
        assertTrue(ba.enthaelt(513));
        assertTrue(ba.enthaelt(378));
        assertTrue(ba.enthaelt(740));
        assertTrue(ba.enthaelt(403));
    }

    @Test
    public void test028()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(27));
        assertEquals(209,ba.anzahl());
        assertEquals(10,ba.tiefe());
        assertFalse(ba.enthaelt(627));
        assertFalse(ba.enthaelt(681));
        assertFalse(ba.enthaelt(521));
        assertFalse(ba.enthaelt(393));
        assertFalse(ba.enthaelt(201));
        assertFalse(ba.enthaelt(364));
        assertFalse(ba.enthaelt(198));
        assertFalse(ba.enthaelt(173));
        assertFalse(ba.enthaelt(257));
        assertFalse(ba.enthaelt(277));
        assertTrue(ba.enthaelt(36));
        assertTrue(ba.enthaelt(829));
        assertTrue(ba.enthaelt(896));
        assertTrue(ba.enthaelt(899));
        assertTrue(ba.enthaelt(706));
        assertTrue(ba.enthaelt(381));
        assertTrue(ba.enthaelt(480));
        assertTrue(ba.enthaelt(926));
        assertTrue(ba.enthaelt(832));
        assertTrue(ba.enthaelt(271));
    }

    @Test
    public void test029()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(28));
        assertEquals(357,ba.anzahl());
        assertEquals(12,ba.tiefe());
        assertFalse(ba.enthaelt(700));
        assertFalse(ba.enthaelt(942));
        assertTrue(ba.enthaelt(924));
        assertTrue(ba.enthaelt(271));
        assertFalse(ba.enthaelt(766));
        assertFalse(ba.enthaelt(528));
        assertFalse(ba.enthaelt(495));
        assertFalse(ba.enthaelt(262));
        assertFalse(ba.enthaelt(413));
        assertTrue(ba.enthaelt(704));
        assertTrue(ba.enthaelt(230));
        assertFalse(ba.enthaelt(752));
        assertFalse(ba.enthaelt(810));
        assertFalse(ba.enthaelt(823));
        assertTrue(ba.enthaelt(846));
        assertTrue(ba.enthaelt(758));
        assertTrue(ba.enthaelt(91));
        assertTrue(ba.enthaelt(995));
        assertTrue(ba.enthaelt(988));
        assertTrue(ba.enthaelt(62));
    }

    @Test
    public void test030()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(29));
        assertEquals(71,ba.anzahl());
        assertEquals(8,ba.tiefe());
        assertFalse(ba.enthaelt(846));
        assertFalse(ba.enthaelt(828));
        assertFalse(ba.enthaelt(444));
        assertFalse(ba.enthaelt(456));
        assertFalse(ba.enthaelt(424));
        assertFalse(ba.enthaelt(454));
        assertFalse(ba.enthaelt(908));
        assertFalse(ba.enthaelt(200));
        assertFalse(ba.enthaelt(463));
        assertFalse(ba.enthaelt(246));
        assertTrue(ba.enthaelt(274));
        assertTrue(ba.enthaelt(553));
        assertTrue(ba.enthaelt(223));
        assertTrue(ba.enthaelt(237));
        assertTrue(ba.enthaelt(557));
        assertTrue(ba.enthaelt(222));
        assertTrue(ba.enthaelt(806));
        assertTrue(ba.enthaelt(516));
        assertTrue(ba.enthaelt(813));
        assertTrue(ba.enthaelt(10));
    }

    @Test
    public void test031()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(30));
        assertEquals(109,ba.anzahl());
        assertEquals(8,ba.tiefe());
        assertFalse(ba.enthaelt(154));
        assertFalse(ba.enthaelt(1000));
        assertTrue(ba.enthaelt(355));
        assertFalse(ba.enthaelt(960));
        assertFalse(ba.enthaelt(301));
        assertFalse(ba.enthaelt(94));
        assertTrue(ba.enthaelt(580));
        assertFalse(ba.enthaelt(440));
        assertFalse(ba.enthaelt(690));
        assertFalse(ba.enthaelt(342));
        assertFalse(ba.enthaelt(71));
        assertFalse(ba.enthaelt(44));
        assertTrue(ba.enthaelt(731));
        assertTrue(ba.enthaelt(617));
        assertTrue(ba.enthaelt(752));
        assertTrue(ba.enthaelt(538));
        assertTrue(ba.enthaelt(122));
        assertTrue(ba.enthaelt(517));
        assertTrue(ba.enthaelt(923));
        assertTrue(ba.enthaelt(689));
    }

    @Test
    public void test032()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(31));
        assertEquals(87,ba.anzahl());
        assertEquals(9,ba.tiefe());
        assertFalse(ba.enthaelt(372));
        assertTrue(ba.enthaelt(866));
        assertTrue(ba.enthaelt(304));
        assertFalse(ba.enthaelt(623));
        assertFalse(ba.enthaelt(261));
        assertFalse(ba.enthaelt(761));
        assertTrue(ba.enthaelt(206));
        assertFalse(ba.enthaelt(459));
        assertFalse(ba.enthaelt(100));
        assertFalse(ba.enthaelt(769));
        assertFalse(ba.enthaelt(134));
        assertFalse(ba.enthaelt(37));
        assertFalse(ba.enthaelt(622));
        assertTrue(ba.enthaelt(61));
        assertTrue(ba.enthaelt(409));
        assertTrue(ba.enthaelt(274));
        assertTrue(ba.enthaelt(30));
        assertTrue(ba.enthaelt(448));
        assertTrue(ba.enthaelt(868));
        assertTrue(ba.enthaelt(36));
    }

    @Test
    public void test033()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(32));
        assertEquals(521,ba.anzahl());
        assertEquals(12,ba.tiefe());
        assertTrue(ba.enthaelt(211));
        assertFalse(ba.enthaelt(231));
        assertTrue(ba.enthaelt(658));
        assertTrue(ba.enthaelt(546));
        assertTrue(ba.enthaelt(53));
        assertTrue(ba.enthaelt(764));
        assertTrue(ba.enthaelt(324));
        assertTrue(ba.enthaelt(851));
        assertFalse(ba.enthaelt(856));
        assertFalse(ba.enthaelt(442));
        assertTrue(ba.enthaelt(15));
        assertFalse(ba.enthaelt(223));
        assertTrue(ba.enthaelt(553));
        assertFalse(ba.enthaelt(782));
        assertFalse(ba.enthaelt(419));
        assertFalse(ba.enthaelt(90));
        assertFalse(ba.enthaelt(221));
        assertFalse(ba.enthaelt(670));
        assertTrue(ba.enthaelt(876));
        assertFalse(ba.enthaelt(902));
    }

    @Test
    public void test034()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(33));
        assertEquals(59,ba.anzahl());
        assertEquals(8,ba.tiefe());
        assertFalse(ba.enthaelt(854));
        assertFalse(ba.enthaelt(673));
        assertFalse(ba.enthaelt(617));
        assertFalse(ba.enthaelt(253));
        assertFalse(ba.enthaelt(298));
        assertTrue(ba.enthaelt(190));
        assertFalse(ba.enthaelt(779));
        assertFalse(ba.enthaelt(994));
        assertFalse(ba.enthaelt(941));
        assertFalse(ba.enthaelt(100));
        assertFalse(ba.enthaelt(351));
        assertTrue(ba.enthaelt(192));
        assertTrue(ba.enthaelt(385));
        assertTrue(ba.enthaelt(581));
        assertTrue(ba.enthaelt(205));
        assertTrue(ba.enthaelt(731));
        assertTrue(ba.enthaelt(230));
        assertTrue(ba.enthaelt(589));
        assertTrue(ba.enthaelt(769));
        assertTrue(ba.enthaelt(705));
    }

    @Test
    public void test035()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(34));
        assertEquals(107,ba.anzahl());
        assertEquals(10,ba.tiefe());
        assertFalse(ba.enthaelt(103));
        assertFalse(ba.enthaelt(238));
        assertFalse(ba.enthaelt(35));
        assertFalse(ba.enthaelt(658));
        assertFalse(ba.enthaelt(134));
        assertFalse(ba.enthaelt(499));
        assertFalse(ba.enthaelt(137));
        assertTrue(ba.enthaelt(243));
        assertFalse(ba.enthaelt(780));
        assertFalse(ba.enthaelt(422));
        assertFalse(ba.enthaelt(930));
        assertTrue(ba.enthaelt(437));
        assertTrue(ba.enthaelt(251));
        assertTrue(ba.enthaelt(802));
        assertTrue(ba.enthaelt(206));
        assertTrue(ba.enthaelt(404));
        assertTrue(ba.enthaelt(79));
        assertTrue(ba.enthaelt(821));
        assertTrue(ba.enthaelt(384));
        assertTrue(ba.enthaelt(665));
    }

    @Test
    public void test036()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(35));
        assertEquals(93,ba.anzahl());
        assertEquals(8,ba.tiefe());
        assertFalse(ba.enthaelt(119));
        assertFalse(ba.enthaelt(413));
        assertFalse(ba.enthaelt(246));
        assertFalse(ba.enthaelt(427));
        assertFalse(ba.enthaelt(660));
        assertFalse(ba.enthaelt(54));
        assertFalse(ba.enthaelt(30));
        assertFalse(ba.enthaelt(417));
        assertFalse(ba.enthaelt(781));
        assertFalse(ba.enthaelt(191));
        assertTrue(ba.enthaelt(93));
        assertTrue(ba.enthaelt(326));
        assertTrue(ba.enthaelt(700));
        assertTrue(ba.enthaelt(339));
        assertTrue(ba.enthaelt(608));
        assertTrue(ba.enthaelt(31));
        assertTrue(ba.enthaelt(576));
        assertTrue(ba.enthaelt(546));
        assertTrue(ba.enthaelt(815));
        assertTrue(ba.enthaelt(196));
    }

    @Test
    public void test037()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(36));
        assertEquals(175,ba.anzahl());
        assertEquals(9,ba.tiefe());
        assertFalse(ba.enthaelt(833));
        assertFalse(ba.enthaelt(408));
        assertFalse(ba.enthaelt(720));
        assertTrue(ba.enthaelt(559));
        assertFalse(ba.enthaelt(839));
        assertFalse(ba.enthaelt(281));
        assertTrue(ba.enthaelt(477));
        assertTrue(ba.enthaelt(920));
        assertFalse(ba.enthaelt(628));
        assertFalse(ba.enthaelt(726));
        assertFalse(ba.enthaelt(659));
        assertFalse(ba.enthaelt(786));
        assertFalse(ba.enthaelt(876));
        assertTrue(ba.enthaelt(33));
        assertTrue(ba.enthaelt(74));
        assertTrue(ba.enthaelt(826));
        assertTrue(ba.enthaelt(901));
        assertTrue(ba.enthaelt(260));
        assertTrue(ba.enthaelt(378));
        assertTrue(ba.enthaelt(782));
    }

    @Test
    public void test038()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(37));
        assertEquals(349,ba.anzahl());
        assertEquals(11,ba.tiefe());
        assertTrue(ba.enthaelt(916));
        assertFalse(ba.enthaelt(947));
        assertFalse(ba.enthaelt(201));
        assertFalse(ba.enthaelt(185));
        assertTrue(ba.enthaelt(255));
        assertFalse(ba.enthaelt(684));
        assertTrue(ba.enthaelt(393));
        assertTrue(ba.enthaelt(88));
        assertFalse(ba.enthaelt(633));
        assertFalse(ba.enthaelt(220));
        assertFalse(ba.enthaelt(334));
        assertFalse(ba.enthaelt(810));
        assertFalse(ba.enthaelt(686));
        assertFalse(ba.enthaelt(579));
        assertTrue(ba.enthaelt(501));
        assertTrue(ba.enthaelt(616));
        assertTrue(ba.enthaelt(389));
        assertTrue(ba.enthaelt(285));
        assertTrue(ba.enthaelt(704));
        assertTrue(ba.enthaelt(253));
    }

    @Test
    public void test039()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(38));
        assertEquals(129,ba.anzahl());
        assertEquals(9,ba.tiefe());
        assertTrue(ba.enthaelt(572));
        assertFalse(ba.enthaelt(391));
        assertFalse(ba.enthaelt(116));
        assertFalse(ba.enthaelt(524));
        assertFalse(ba.enthaelt(569));
        assertFalse(ba.enthaelt(1));
        assertFalse(ba.enthaelt(794));
        assertFalse(ba.enthaelt(300));
        assertFalse(ba.enthaelt(865));
        assertFalse(ba.enthaelt(209));
        assertFalse(ba.enthaelt(408));
        assertTrue(ba.enthaelt(99));
        assertTrue(ba.enthaelt(154));
        assertTrue(ba.enthaelt(734));
        assertTrue(ba.enthaelt(177));
        assertTrue(ba.enthaelt(100));
        assertTrue(ba.enthaelt(536));
        assertTrue(ba.enthaelt(999));
        assertTrue(ba.enthaelt(284));
        assertTrue(ba.enthaelt(628));
    }

    @Test
    public void test040()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(39));
        assertEquals(281,ba.anzahl());
        assertEquals(11,ba.tiefe());
        assertFalse(ba.enthaelt(562));
        assertFalse(ba.enthaelt(19));
        assertFalse(ba.enthaelt(848));
        assertFalse(ba.enthaelt(681));
        assertFalse(ba.enthaelt(878));
        assertFalse(ba.enthaelt(407));
        assertTrue(ba.enthaelt(38));
        assertFalse(ba.enthaelt(641));
        assertTrue(ba.enthaelt(634));
        assertFalse(ba.enthaelt(577));
        assertFalse(ba.enthaelt(976));
        assertFalse(ba.enthaelt(300));
        assertTrue(ba.enthaelt(760));
        assertTrue(ba.enthaelt(476));
        assertTrue(ba.enthaelt(899));
        assertTrue(ba.enthaelt(851));
        assertTrue(ba.enthaelt(618));
        assertTrue(ba.enthaelt(808));
        assertTrue(ba.enthaelt(244));
        assertTrue(ba.enthaelt(401));
    }

    @Test
    public void test041()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(40));
        assertEquals(117,ba.anzahl());
        assertEquals(9,ba.tiefe());
        assertTrue(ba.enthaelt(517));
        assertFalse(ba.enthaelt(883));
        assertFalse(ba.enthaelt(382));
        assertFalse(ba.enthaelt(1008));
        assertFalse(ba.enthaelt(949));
        assertFalse(ba.enthaelt(24));
        assertFalse(ba.enthaelt(207));
        assertFalse(ba.enthaelt(147));
        assertTrue(ba.enthaelt(733));
        assertTrue(ba.enthaelt(847));
        assertFalse(ba.enthaelt(741));
        assertFalse(ba.enthaelt(868));
        assertFalse(ba.enthaelt(785));
        assertTrue(ba.enthaelt(544));
        assertTrue(ba.enthaelt(302));
        assertTrue(ba.enthaelt(797));
        assertTrue(ba.enthaelt(449));
        assertTrue(ba.enthaelt(620));
        assertTrue(ba.enthaelt(465));
        assertTrue(ba.enthaelt(587));
    }

    @Test
    public void test042()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(41));
        assertEquals(97,ba.anzahl());
        assertEquals(8,ba.tiefe());
        assertFalse(ba.enthaelt(201));
        assertFalse(ba.enthaelt(293));
        assertFalse(ba.enthaelt(287));
        assertTrue(ba.enthaelt(627));
        assertTrue(ba.enthaelt(225));
        assertFalse(ba.enthaelt(224));
        assertTrue(ba.enthaelt(994));
        assertFalse(ba.enthaelt(177));
        assertFalse(ba.enthaelt(644));
        assertFalse(ba.enthaelt(935));
        assertFalse(ba.enthaelt(1006));
        assertFalse(ba.enthaelt(488));
        assertFalse(ba.enthaelt(746));
        assertTrue(ba.enthaelt(179));
        assertTrue(ba.enthaelt(1));
        assertTrue(ba.enthaelt(903));
        assertTrue(ba.enthaelt(522));
        assertTrue(ba.enthaelt(407));
        assertTrue(ba.enthaelt(313));
        assertTrue(ba.enthaelt(156));
    }

    @Test
    public void test043()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(42));
        assertEquals(157,ba.anzahl());
        assertEquals(8,ba.tiefe());
        assertFalse(ba.enthaelt(608));
        assertTrue(ba.enthaelt(375));
        assertFalse(ba.enthaelt(689));
        assertFalse(ba.enthaelt(856));
        assertTrue(ba.enthaelt(139));
        assertFalse(ba.enthaelt(693));
        assertFalse(ba.enthaelt(887));
        assertFalse(ba.enthaelt(481));
        assertFalse(ba.enthaelt(256));
        assertFalse(ba.enthaelt(182));
        assertFalse(ba.enthaelt(751));
        assertFalse(ba.enthaelt(114));
        assertTrue(ba.enthaelt(875));
        assertTrue(ba.enthaelt(516));
        assertTrue(ba.enthaelt(838));
        assertTrue(ba.enthaelt(744));
        assertTrue(ba.enthaelt(78));
        assertTrue(ba.enthaelt(797));
        assertTrue(ba.enthaelt(362));
        assertTrue(ba.enthaelt(755));
    }

    @Test
    public void test044()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(43));
        assertEquals(347,ba.anzahl());
        assertEquals(11,ba.tiefe());
        assertFalse(ba.enthaelt(357));
        assertFalse(ba.enthaelt(718));
        assertTrue(ba.enthaelt(756));
        assertTrue(ba.enthaelt(58));
        assertFalse(ba.enthaelt(313));
        assertFalse(ba.enthaelt(530));
        assertTrue(ba.enthaelt(902));
        assertFalse(ba.enthaelt(402));
        assertTrue(ba.enthaelt(197));
        assertFalse(ba.enthaelt(854));
        assertTrue(ba.enthaelt(751));
        assertFalse(ba.enthaelt(942));
        assertFalse(ba.enthaelt(450));
        assertFalse(ba.enthaelt(864));
        assertTrue(ba.enthaelt(220));
        assertFalse(ba.enthaelt(835));
        assertTrue(ba.enthaelt(191));
        assertTrue(ba.enthaelt(859));
        assertTrue(ba.enthaelt(793));
        assertTrue(ba.enthaelt(237));
    }

    @Test
    public void test045()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(44));
        assertEquals(459,ba.anzahl());
        assertEquals(12,ba.tiefe());
        assertFalse(ba.enthaelt(126));
        assertFalse(ba.enthaelt(328));
        assertTrue(ba.enthaelt(305));
        assertFalse(ba.enthaelt(597));
        assertFalse(ba.enthaelt(0));
        assertFalse(ba.enthaelt(747));
        assertFalse(ba.enthaelt(1019));
        assertFalse(ba.enthaelt(93));
        assertFalse(ba.enthaelt(866));
        assertTrue(ba.enthaelt(271));
        assertFalse(ba.enthaelt(81));
        assertFalse(ba.enthaelt(228));
        assertTrue(ba.enthaelt(241));
        assertTrue(ba.enthaelt(335));
        assertTrue(ba.enthaelt(257));
        assertTrue(ba.enthaelt(83));
        assertTrue(ba.enthaelt(885));
        assertTrue(ba.enthaelt(303));
        assertTrue(ba.enthaelt(498));
        assertTrue(ba.enthaelt(366));
    }

    @Test
    public void test046()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(45));
        assertEquals(187,ba.anzahl());
        assertEquals(9,ba.tiefe());
        assertFalse(ba.enthaelt(916));
        assertFalse(ba.enthaelt(489));
        assertFalse(ba.enthaelt(370));
        assertFalse(ba.enthaelt(361));
        assertFalse(ba.enthaelt(635));
        assertFalse(ba.enthaelt(991));
        assertTrue(ba.enthaelt(308));
        assertFalse(ba.enthaelt(677));
        assertTrue(ba.enthaelt(203));
        assertFalse(ba.enthaelt(818));
        assertFalse(ba.enthaelt(175));
        assertFalse(ba.enthaelt(992));
        assertTrue(ba.enthaelt(72));
        assertTrue(ba.enthaelt(516));
        assertTrue(ba.enthaelt(127));
        assertTrue(ba.enthaelt(357));
        assertTrue(ba.enthaelt(535));
        assertTrue(ba.enthaelt(623));
        assertTrue(ba.enthaelt(526));
        assertTrue(ba.enthaelt(483));
    }

    @Test
    public void test047()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(46));
        assertEquals(119,ba.anzahl());
        assertEquals(9,ba.tiefe());
        assertFalse(ba.enthaelt(174));
        assertFalse(ba.enthaelt(700));
        assertFalse(ba.enthaelt(172));
        assertFalse(ba.enthaelt(679));
        assertFalse(ba.enthaelt(459));
        assertFalse(ba.enthaelt(522));
        assertFalse(ba.enthaelt(552));
        assertFalse(ba.enthaelt(1001));
        assertFalse(ba.enthaelt(448));
        assertFalse(ba.enthaelt(578));
        assertTrue(ba.enthaelt(157));
        assertTrue(ba.enthaelt(830));
        assertTrue(ba.enthaelt(316));
        assertTrue(ba.enthaelt(504));
        assertTrue(ba.enthaelt(307));
        assertTrue(ba.enthaelt(516));
        assertTrue(ba.enthaelt(533));
        assertTrue(ba.enthaelt(378));
        assertTrue(ba.enthaelt(151));
        assertTrue(ba.enthaelt(23));
    }

    @Test
    public void test048()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(47));
        assertEquals(81,ba.anzahl());
        assertEquals(9,ba.tiefe());
        assertFalse(ba.enthaelt(763));
        assertFalse(ba.enthaelt(697));
        assertFalse(ba.enthaelt(24));
        assertFalse(ba.enthaelt(516));
        assertFalse(ba.enthaelt(997));
        assertFalse(ba.enthaelt(874));
        assertFalse(ba.enthaelt(331));
        assertFalse(ba.enthaelt(602));
        assertFalse(ba.enthaelt(952));
        assertFalse(ba.enthaelt(928));
        assertTrue(ba.enthaelt(794));
        assertTrue(ba.enthaelt(342));
        assertTrue(ba.enthaelt(551));
        assertTrue(ba.enthaelt(600));
        assertTrue(ba.enthaelt(532));
        assertTrue(ba.enthaelt(86));
        assertTrue(ba.enthaelt(217));
        assertTrue(ba.enthaelt(921));
        assertTrue(ba.enthaelt(115));
        assertTrue(ba.enthaelt(32));
    }

    @Test
    public void test049()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(48));
        assertEquals(349,ba.anzahl());
        assertEquals(11,ba.tiefe());
        assertTrue(ba.enthaelt(394));
        assertFalse(ba.enthaelt(690));
        assertFalse(ba.enthaelt(3));
        assertTrue(ba.enthaelt(400));
        assertFalse(ba.enthaelt(264));
        assertFalse(ba.enthaelt(823));
        assertTrue(ba.enthaelt(569));
        assertFalse(ba.enthaelt(140));
        assertFalse(ba.enthaelt(1008));
        assertTrue(ba.enthaelt(42));
        assertFalse(ba.enthaelt(818));
        assertFalse(ba.enthaelt(902));
        assertTrue(ba.enthaelt(41));
        assertFalse(ba.enthaelt(631));
        assertFalse(ba.enthaelt(927));
        assertTrue(ba.enthaelt(790));
        assertTrue(ba.enthaelt(464));
        assertTrue(ba.enthaelt(447));
        assertTrue(ba.enthaelt(203));
        assertTrue(ba.enthaelt(774));
    }

    @Test
    public void test050()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(49));
        assertEquals(73,ba.anzahl());
        assertEquals(8,ba.tiefe());
        assertTrue(ba.enthaelt(942));
        assertFalse(ba.enthaelt(310));
        assertFalse(ba.enthaelt(44));
        assertFalse(ba.enthaelt(536));
        assertFalse(ba.enthaelt(512));
        assertTrue(ba.enthaelt(257));
        assertFalse(ba.enthaelt(219));
        assertFalse(ba.enthaelt(73));
        assertFalse(ba.enthaelt(474));
        assertTrue(ba.enthaelt(21));
        assertFalse(ba.enthaelt(820));
        assertFalse(ba.enthaelt(358));
        assertFalse(ba.enthaelt(651));
        assertTrue(ba.enthaelt(97));
        assertTrue(ba.enthaelt(309));
        assertTrue(ba.enthaelt(815));
        assertTrue(ba.enthaelt(914));
        assertTrue(ba.enthaelt(959));
        assertTrue(ba.enthaelt(183));
        assertTrue(ba.enthaelt(502));
    }

    @Test
    public void test051()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(50));
        assertEquals(115,ba.anzahl());
        assertEquals(8,ba.tiefe());
        assertFalse(ba.enthaelt(985));
        assertFalse(ba.enthaelt(199));
        assertFalse(ba.enthaelt(872));
        assertFalse(ba.enthaelt(721));
        assertFalse(ba.enthaelt(396));
        assertFalse(ba.enthaelt(318));
        assertFalse(ba.enthaelt(625));
        assertFalse(ba.enthaelt(762));
        assertFalse(ba.enthaelt(324));
        assertFalse(ba.enthaelt(913));
        assertTrue(ba.enthaelt(869));
        assertTrue(ba.enthaelt(541));
        assertTrue(ba.enthaelt(444));
        assertTrue(ba.enthaelt(937));
        assertTrue(ba.enthaelt(837));
        assertTrue(ba.enthaelt(898));
        assertTrue(ba.enthaelt(843));
        assertTrue(ba.enthaelt(236));
        assertTrue(ba.enthaelt(954));
        assertTrue(ba.enthaelt(752));
    }

    @Test
    public void test052()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(51));
        assertEquals(437,ba.anzahl());
        assertEquals(12,ba.tiefe());
        assertFalse(ba.enthaelt(352));
        assertTrue(ba.enthaelt(305));
        assertFalse(ba.enthaelt(708));
        assertTrue(ba.enthaelt(670));
        assertTrue(ba.enthaelt(281));
        assertFalse(ba.enthaelt(677));
        assertTrue(ba.enthaelt(410));
        assertFalse(ba.enthaelt(1015));
        assertFalse(ba.enthaelt(555));
        assertFalse(ba.enthaelt(1004));
        assertFalse(ba.enthaelt(768));
        assertFalse(ba.enthaelt(774));
        assertTrue(ba.enthaelt(491));
        assertTrue(ba.enthaelt(589));
        assertFalse(ba.enthaelt(360));
        assertFalse(ba.enthaelt(720));
        assertTrue(ba.enthaelt(779));
        assertTrue(ba.enthaelt(871));
        assertTrue(ba.enthaelt(538));
        assertTrue(ba.enthaelt(918));
    }

    @Test
    public void test053()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(52));
        assertEquals(209,ba.anzahl());
        assertEquals(9,ba.tiefe());
        assertFalse(ba.enthaelt(719));
        assertFalse(ba.enthaelt(933));
        assertFalse(ba.enthaelt(77));
        assertFalse(ba.enthaelt(844));
        assertFalse(ba.enthaelt(744));
        assertFalse(ba.enthaelt(1008));
        assertFalse(ba.enthaelt(208));
        assertFalse(ba.enthaelt(17));
        assertFalse(ba.enthaelt(171));
        assertFalse(ba.enthaelt(290));
        assertTrue(ba.enthaelt(44));
        assertTrue(ba.enthaelt(780));
        assertTrue(ba.enthaelt(303));
        assertTrue(ba.enthaelt(343));
        assertTrue(ba.enthaelt(177));
        assertTrue(ba.enthaelt(488));
        assertTrue(ba.enthaelt(85));
        assertTrue(ba.enthaelt(574));
        assertTrue(ba.enthaelt(765));
        assertTrue(ba.enthaelt(150));
    }

    @Test
    public void test054()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(53));
        assertEquals(151,ba.anzahl());
        assertEquals(11,ba.tiefe());
        assertFalse(ba.enthaelt(272));
        assertTrue(ba.enthaelt(254));
        assertFalse(ba.enthaelt(72));
        assertTrue(ba.enthaelt(739));
        assertTrue(ba.enthaelt(119));
        assertFalse(ba.enthaelt(605));
        assertFalse(ba.enthaelt(438));
        assertTrue(ba.enthaelt(738));
        assertFalse(ba.enthaelt(3));
        assertFalse(ba.enthaelt(27));
        assertFalse(ba.enthaelt(572));
        assertFalse(ba.enthaelt(1003));
        assertFalse(ba.enthaelt(971));
        assertFalse(ba.enthaelt(283));
        assertTrue(ba.enthaelt(566));
        assertTrue(ba.enthaelt(159));
        assertTrue(ba.enthaelt(505));
        assertTrue(ba.enthaelt(755));
        assertTrue(ba.enthaelt(540));
        assertTrue(ba.enthaelt(126));
    }

    @Test
    public void test055()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(54));
        assertEquals(311,ba.anzahl());
        assertEquals(12,ba.tiefe());
        assertTrue(ba.enthaelt(619));
        assertTrue(ba.enthaelt(583));
        assertTrue(ba.enthaelt(258));
        assertFalse(ba.enthaelt(855));
        assertFalse(ba.enthaelt(436));
        assertTrue(ba.enthaelt(182));
        assertTrue(ba.enthaelt(587));
        assertTrue(ba.enthaelt(880));
        assertFalse(ba.enthaelt(883));
        assertFalse(ba.enthaelt(225));
        assertFalse(ba.enthaelt(460));
        assertFalse(ba.enthaelt(501));
        assertFalse(ba.enthaelt(286));
        assertFalse(ba.enthaelt(727));
        assertFalse(ba.enthaelt(589));
        assertFalse(ba.enthaelt(773));
        assertTrue(ba.enthaelt(912));
        assertTrue(ba.enthaelt(671));
        assertTrue(ba.enthaelt(245));
        assertTrue(ba.enthaelt(145));
    }

    @Test
    public void test056()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(55));
        assertEquals(93,ba.anzahl());
        assertEquals(10,ba.tiefe());
        assertTrue(ba.enthaelt(232));
        assertFalse(ba.enthaelt(504));
        assertFalse(ba.enthaelt(951));
        assertFalse(ba.enthaelt(628));
        assertFalse(ba.enthaelt(292));
        assertFalse(ba.enthaelt(575));
        assertFalse(ba.enthaelt(188));
        assertFalse(ba.enthaelt(255));
        assertFalse(ba.enthaelt(913));
        assertFalse(ba.enthaelt(592));
        assertFalse(ba.enthaelt(649));
        assertTrue(ba.enthaelt(897));
        assertTrue(ba.enthaelt(385));
        assertTrue(ba.enthaelt(128));
        assertTrue(ba.enthaelt(228));
        assertTrue(ba.enthaelt(430));
        assertTrue(ba.enthaelt(653));
        assertTrue(ba.enthaelt(497));
        assertTrue(ba.enthaelt(317));
        assertTrue(ba.enthaelt(269));
    }

    @Test
    public void test057()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(56));
        assertEquals(499,ba.anzahl());
        assertEquals(11,ba.tiefe());
        assertTrue(ba.enthaelt(83));
        assertFalse(ba.enthaelt(557));
        assertFalse(ba.enthaelt(863));
        assertTrue(ba.enthaelt(569));
        assertTrue(ba.enthaelt(116));
        assertFalse(ba.enthaelt(841));
        assertTrue(ba.enthaelt(683));
        assertFalse(ba.enthaelt(450));
        assertFalse(ba.enthaelt(783));
        assertTrue(ba.enthaelt(49));
        assertFalse(ba.enthaelt(87));
        assertTrue(ba.enthaelt(983));
        assertTrue(ba.enthaelt(810));
        assertTrue(ba.enthaelt(354));
        assertFalse(ba.enthaelt(155));
        assertFalse(ba.enthaelt(2));
        assertFalse(ba.enthaelt(725));
        assertTrue(ba.enthaelt(786));
        assertTrue(ba.enthaelt(916));
        assertFalse(ba.enthaelt(1009));
    }

    @Test
    public void test058()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(57));
        assertEquals(243,ba.anzahl());
        assertEquals(10,ba.tiefe());
        assertTrue(ba.enthaelt(737));
        assertFalse(ba.enthaelt(969));
        assertFalse(ba.enthaelt(1004));
        assertTrue(ba.enthaelt(633));
        assertFalse(ba.enthaelt(993));
        assertFalse(ba.enthaelt(367));
        assertFalse(ba.enthaelt(743));
        assertFalse(ba.enthaelt(998));
        assertFalse(ba.enthaelt(841));
        assertFalse(ba.enthaelt(775));
        assertTrue(ba.enthaelt(365));
        assertFalse(ba.enthaelt(1018));
        assertFalse(ba.enthaelt(517));
        assertTrue(ba.enthaelt(242));
        assertTrue(ba.enthaelt(100));
        assertTrue(ba.enthaelt(404));
        assertTrue(ba.enthaelt(635));
        assertTrue(ba.enthaelt(337));
        assertTrue(ba.enthaelt(347));
        assertTrue(ba.enthaelt(133));
    }

    @Test
    public void test059()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(58));
        assertEquals(875,ba.anzahl());
        assertEquals(12,ba.tiefe());
        assertTrue(ba.enthaelt(665));
        assertFalse(ba.enthaelt(774));
        assertFalse(ba.enthaelt(855));
        assertFalse(ba.enthaelt(490));
        assertFalse(ba.enthaelt(690));
        assertFalse(ba.enthaelt(975));
        assertTrue(ba.enthaelt(385));
        assertTrue(ba.enthaelt(789));
        assertFalse(ba.enthaelt(625));
        assertTrue(ba.enthaelt(569));
        assertTrue(ba.enthaelt(818));
        assertFalse(ba.enthaelt(442));
        assertTrue(ba.enthaelt(826));
        assertTrue(ba.enthaelt(566));
        assertFalse(ba.enthaelt(1001));
        assertFalse(ba.enthaelt(886));
        assertTrue(ba.enthaelt(49));
        assertFalse(ba.enthaelt(463));
        assertTrue(ba.enthaelt(29));
        assertTrue(ba.enthaelt(84));
    }

    @Test
    public void test060()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(59));
        assertEquals(251,ba.anzahl());
        assertEquals(11,ba.tiefe());
        assertTrue(ba.enthaelt(81));
        assertFalse(ba.enthaelt(447));
        assertTrue(ba.enthaelt(998));
        assertFalse(ba.enthaelt(0));
        assertFalse(ba.enthaelt(1000));
        assertFalse(ba.enthaelt(118));
        assertFalse(ba.enthaelt(86));
        assertFalse(ba.enthaelt(570));
        assertFalse(ba.enthaelt(104));
        assertTrue(ba.enthaelt(495));
        assertFalse(ba.enthaelt(584));
        assertFalse(ba.enthaelt(560));
        assertTrue(ba.enthaelt(623));
        assertFalse(ba.enthaelt(1005));
        assertTrue(ba.enthaelt(740));
        assertTrue(ba.enthaelt(360));
        assertTrue(ba.enthaelt(630));
        assertTrue(ba.enthaelt(264));
        assertTrue(ba.enthaelt(220));
        assertTrue(ba.enthaelt(469));
    }

    @Test
    public void test061()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(60));
        assertEquals(97,ba.anzahl());
        assertEquals(8,ba.tiefe());
        assertFalse(ba.enthaelt(432));
        assertFalse(ba.enthaelt(345));
        assertFalse(ba.enthaelt(594));
        assertFalse(ba.enthaelt(752));
        assertFalse(ba.enthaelt(751));
        assertFalse(ba.enthaelt(998));
        assertTrue(ba.enthaelt(9));
        assertFalse(ba.enthaelt(940));
        assertFalse(ba.enthaelt(1002));
        assertFalse(ba.enthaelt(296));
        assertFalse(ba.enthaelt(951));
        assertTrue(ba.enthaelt(169));
        assertTrue(ba.enthaelt(392));
        assertTrue(ba.enthaelt(226));
        assertTrue(ba.enthaelt(885));
        assertTrue(ba.enthaelt(350));
        assertTrue(ba.enthaelt(799));
        assertTrue(ba.enthaelt(877));
        assertTrue(ba.enthaelt(180));
        assertTrue(ba.enthaelt(836));
    }

    @Test
    public void test062()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(61));
        assertEquals(85,ba.anzahl());
        assertEquals(8,ba.tiefe());
        assertFalse(ba.enthaelt(1019));
        assertFalse(ba.enthaelt(997));
        assertFalse(ba.enthaelt(340));
        assertFalse(ba.enthaelt(594));
        assertFalse(ba.enthaelt(996));
        assertFalse(ba.enthaelt(766));
        assertFalse(ba.enthaelt(241));
        assertFalse(ba.enthaelt(192));
        assertFalse(ba.enthaelt(740));
        assertFalse(ba.enthaelt(869));
        assertTrue(ba.enthaelt(75));
        assertTrue(ba.enthaelt(3));
        assertTrue(ba.enthaelt(634));
        assertTrue(ba.enthaelt(814));
        assertTrue(ba.enthaelt(438));
        assertTrue(ba.enthaelt(785));
        assertTrue(ba.enthaelt(755));
        assertTrue(ba.enthaelt(446));
        assertTrue(ba.enthaelt(414));
        assertTrue(ba.enthaelt(579));
    }

    @Test
    public void test063()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(62));
        assertEquals(221,ba.anzahl());
        assertEquals(12,ba.tiefe());
        assertFalse(ba.enthaelt(251));
        assertFalse(ba.enthaelt(138));
        assertTrue(ba.enthaelt(803));
        assertFalse(ba.enthaelt(280));
        assertFalse(ba.enthaelt(62));
        assertFalse(ba.enthaelt(665));
        assertFalse(ba.enthaelt(429));
        assertTrue(ba.enthaelt(524));
        assertFalse(ba.enthaelt(656));
        assertTrue(ba.enthaelt(463));
        assertFalse(ba.enthaelt(131));
        assertFalse(ba.enthaelt(759));
        assertFalse(ba.enthaelt(26));
        assertTrue(ba.enthaelt(303));
        assertTrue(ba.enthaelt(213));
        assertTrue(ba.enthaelt(167));
        assertTrue(ba.enthaelt(399));
        assertTrue(ba.enthaelt(264));
        assertTrue(ba.enthaelt(944));
        assertTrue(ba.enthaelt(625));
    }

    @Test
    public void test064()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(63));
        assertEquals(115,ba.anzahl());
        assertEquals(9,ba.tiefe());
        assertTrue(ba.enthaelt(690));
        assertFalse(ba.enthaelt(204));
        assertFalse(ba.enthaelt(525));
        assertFalse(ba.enthaelt(448));
        assertFalse(ba.enthaelt(658));
        assertFalse(ba.enthaelt(628));
        assertFalse(ba.enthaelt(444));
        assertFalse(ba.enthaelt(28));
        assertFalse(ba.enthaelt(622));
        assertFalse(ba.enthaelt(270));
        assertFalse(ba.enthaelt(731));
        assertTrue(ba.enthaelt(38));
        assertTrue(ba.enthaelt(479));
        assertTrue(ba.enthaelt(182));
        assertTrue(ba.enthaelt(24));
        assertTrue(ba.enthaelt(754));
        assertTrue(ba.enthaelt(308));
        assertTrue(ba.enthaelt(838));
        assertTrue(ba.enthaelt(85));
        assertTrue(ba.enthaelt(289));
    }

    @Test
    public void test065()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(64));
        assertEquals(513,ba.anzahl());
        assertEquals(12,ba.tiefe());
        assertFalse(ba.enthaelt(579));
        assertFalse(ba.enthaelt(140));
        assertFalse(ba.enthaelt(598));
        assertFalse(ba.enthaelt(303));
        assertTrue(ba.enthaelt(524));
        assertFalse(ba.enthaelt(776));
        assertFalse(ba.enthaelt(522));
        assertTrue(ba.enthaelt(323));
        assertFalse(ba.enthaelt(168));
        assertTrue(ba.enthaelt(787));
        assertFalse(ba.enthaelt(922));
        assertFalse(ba.enthaelt(3));
        assertFalse(ba.enthaelt(1007));
        assertTrue(ba.enthaelt(900));
        assertTrue(ba.enthaelt(988));
        assertTrue(ba.enthaelt(113));
        assertTrue(ba.enthaelt(274));
        assertTrue(ba.enthaelt(653));
        assertTrue(ba.enthaelt(958));
        assertTrue(ba.enthaelt(866));
    }

    @Test
    public void test066()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(65));
        assertEquals(79,ba.anzahl());
        assertEquals(8,ba.tiefe());
        assertFalse(ba.enthaelt(21));
        assertFalse(ba.enthaelt(658));
        assertFalse(ba.enthaelt(843));
        assertFalse(ba.enthaelt(441));
        assertTrue(ba.enthaelt(976));
        assertFalse(ba.enthaelt(797));
        assertFalse(ba.enthaelt(986));
        assertFalse(ba.enthaelt(159));
        assertFalse(ba.enthaelt(305));
        assertFalse(ba.enthaelt(486));
        assertFalse(ba.enthaelt(290));
        assertTrue(ba.enthaelt(221));
        assertTrue(ba.enthaelt(216));
        assertTrue(ba.enthaelt(696));
        assertTrue(ba.enthaelt(517));
        assertTrue(ba.enthaelt(898));
        assertTrue(ba.enthaelt(820));
        assertTrue(ba.enthaelt(243));
        assertTrue(ba.enthaelt(874));
        assertTrue(ba.enthaelt(225));
    }

    @Test
    public void test067()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(66));
        assertEquals(145,ba.anzahl());
        assertEquals(10,ba.tiefe());
        assertFalse(ba.enthaelt(588));
        assertFalse(ba.enthaelt(860));
        assertTrue(ba.enthaelt(451));
        assertFalse(ba.enthaelt(461));
        assertFalse(ba.enthaelt(634));
        assertFalse(ba.enthaelt(154));
        assertTrue(ba.enthaelt(146));
        assertFalse(ba.enthaelt(581));
        assertFalse(ba.enthaelt(778));
        assertFalse(ba.enthaelt(986));
        assertFalse(ba.enthaelt(580));
        assertFalse(ba.enthaelt(685));
        assertTrue(ba.enthaelt(305));
        assertTrue(ba.enthaelt(669));
        assertTrue(ba.enthaelt(106));
        assertTrue(ba.enthaelt(635));
        assertTrue(ba.enthaelt(173));
        assertTrue(ba.enthaelt(536));
        assertTrue(ba.enthaelt(249));
        assertTrue(ba.enthaelt(244));
    }

    @Test
    public void test068()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(67));
        assertEquals(653,ba.anzahl());
        assertEquals(12,ba.tiefe());
        assertTrue(ba.enthaelt(950));
        assertFalse(ba.enthaelt(762));
        assertFalse(ba.enthaelt(499));
        assertFalse(ba.enthaelt(543));
        assertTrue(ba.enthaelt(161));
        assertFalse(ba.enthaelt(288));
        assertFalse(ba.enthaelt(135));
        assertTrue(ba.enthaelt(119));
        assertTrue(ba.enthaelt(379));
        assertFalse(ba.enthaelt(9));
        assertFalse(ba.enthaelt(854));
        assertFalse(ba.enthaelt(634));
        assertTrue(ba.enthaelt(215));
        assertFalse(ba.enthaelt(345));
        assertTrue(ba.enthaelt(326));
        assertFalse(ba.enthaelt(948));
        assertTrue(ba.enthaelt(328));
        assertTrue(ba.enthaelt(803));
        assertTrue(ba.enthaelt(463));
        assertTrue(ba.enthaelt(212));
    }

    @Test
    public void test069()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(68));
        assertEquals(231,ba.anzahl());
        assertEquals(12,ba.tiefe());
        assertFalse(ba.enthaelt(450));
        assertFalse(ba.enthaelt(942));
        assertFalse(ba.enthaelt(343));
        assertFalse(ba.enthaelt(195));
        assertFalse(ba.enthaelt(521));
        assertFalse(ba.enthaelt(377));
        assertFalse(ba.enthaelt(839));
        assertFalse(ba.enthaelt(727));
        assertFalse(ba.enthaelt(643));
        assertFalse(ba.enthaelt(750));
        assertTrue(ba.enthaelt(422));
        assertTrue(ba.enthaelt(569));
        assertTrue(ba.enthaelt(852));
        assertTrue(ba.enthaelt(808));
        assertTrue(ba.enthaelt(771));
        assertTrue(ba.enthaelt(874));
        assertTrue(ba.enthaelt(49));
        assertTrue(ba.enthaelt(308));
        assertTrue(ba.enthaelt(176));
        assertTrue(ba.enthaelt(198));
    }

    @Test
    public void test070()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(69));
        assertEquals(241,ba.anzahl());
        assertEquals(11,ba.tiefe());
        assertFalse(ba.enthaelt(982));
        assertFalse(ba.enthaelt(326));
        assertTrue(ba.enthaelt(925));
        assertTrue(ba.enthaelt(680));
        assertTrue(ba.enthaelt(71));
        assertFalse(ba.enthaelt(494));
        assertFalse(ba.enthaelt(841));
        assertFalse(ba.enthaelt(915));
        assertTrue(ba.enthaelt(100));
        assertFalse(ba.enthaelt(215));
        assertFalse(ba.enthaelt(48));
        assertFalse(ba.enthaelt(5));
        assertFalse(ba.enthaelt(891));
        assertFalse(ba.enthaelt(372));
        assertTrue(ba.enthaelt(908));
        assertTrue(ba.enthaelt(451));
        assertTrue(ba.enthaelt(258));
        assertTrue(ba.enthaelt(473));
        assertTrue(ba.enthaelt(861));
        assertTrue(ba.enthaelt(734));
    }

    @Test
    public void test071()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(70));
        assertEquals(147,ba.anzahl());
        assertEquals(11,ba.tiefe());
        assertFalse(ba.enthaelt(803));
        assertFalse(ba.enthaelt(898));
        assertFalse(ba.enthaelt(464));
        assertFalse(ba.enthaelt(12));
        assertFalse(ba.enthaelt(439));
        assertFalse(ba.enthaelt(673));
        assertFalse(ba.enthaelt(750));
        assertTrue(ba.enthaelt(494));
        assertFalse(ba.enthaelt(1007));
        assertFalse(ba.enthaelt(119));
        assertFalse(ba.enthaelt(113));
        assertTrue(ba.enthaelt(588));
        assertTrue(ba.enthaelt(829));
        assertTrue(ba.enthaelt(617));
        assertTrue(ba.enthaelt(644));
        assertTrue(ba.enthaelt(537));
        assertTrue(ba.enthaelt(471));
        assertTrue(ba.enthaelt(70));
        assertTrue(ba.enthaelt(769));
        assertTrue(ba.enthaelt(581));
    }

    @Test
    public void test072()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(71));
        assertEquals(165,ba.anzahl());
        assertEquals(10,ba.tiefe());
        assertTrue(ba.enthaelt(447));
        assertTrue(ba.enthaelt(675));
        assertFalse(ba.enthaelt(172));
        assertFalse(ba.enthaelt(438));
        assertTrue(ba.enthaelt(105));
        assertFalse(ba.enthaelt(198));
        assertFalse(ba.enthaelt(328));
        assertFalse(ba.enthaelt(86));
        assertFalse(ba.enthaelt(293));
        assertFalse(ba.enthaelt(441));
        assertFalse(ba.enthaelt(466));
        assertFalse(ba.enthaelt(572));
        assertTrue(ba.enthaelt(499));
        assertFalse(ba.enthaelt(218));
        assertTrue(ba.enthaelt(518));
        assertTrue(ba.enthaelt(813));
        assertTrue(ba.enthaelt(32));
        assertTrue(ba.enthaelt(554));
        assertTrue(ba.enthaelt(152));
        assertTrue(ba.enthaelt(19));
    }

    @Test
    public void test073()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(72));
        assertEquals(231,ba.anzahl());
        assertEquals(10,ba.tiefe());
        assertFalse(ba.enthaelt(891));
        assertFalse(ba.enthaelt(908));
        assertTrue(ba.enthaelt(138));
        assertFalse(ba.enthaelt(1012));
        assertFalse(ba.enthaelt(189));
        assertTrue(ba.enthaelt(745));
        assertFalse(ba.enthaelt(799));
        assertTrue(ba.enthaelt(922));
        assertTrue(ba.enthaelt(595));
        assertFalse(ba.enthaelt(391));
        assertFalse(ba.enthaelt(31));
        assertFalse(ba.enthaelt(177));
        assertFalse(ba.enthaelt(673));
        assertFalse(ba.enthaelt(348));
        assertTrue(ba.enthaelt(850));
        assertTrue(ba.enthaelt(162));
        assertTrue(ba.enthaelt(820));
        assertTrue(ba.enthaelt(219));
        assertTrue(ba.enthaelt(388));
        assertTrue(ba.enthaelt(694));
    }

    @Test
    public void test074()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(73));
        assertEquals(191,ba.anzahl());
        assertEquals(10,ba.tiefe());
        assertFalse(ba.enthaelt(92));
        assertFalse(ba.enthaelt(929));
        assertTrue(ba.enthaelt(312));
        assertFalse(ba.enthaelt(209));
        assertFalse(ba.enthaelt(843));
        assertFalse(ba.enthaelt(935));
        assertFalse(ba.enthaelt(6));
        assertFalse(ba.enthaelt(529));
        assertFalse(ba.enthaelt(567));
        assertFalse(ba.enthaelt(646));
        assertFalse(ba.enthaelt(116));
        assertTrue(ba.enthaelt(788));
        assertTrue(ba.enthaelt(487));
        assertTrue(ba.enthaelt(330));
        assertTrue(ba.enthaelt(813));
        assertTrue(ba.enthaelt(77));
        assertTrue(ba.enthaelt(624));
        assertTrue(ba.enthaelt(722));
        assertTrue(ba.enthaelt(566));
        assertTrue(ba.enthaelt(215));
    }

    @Test
    public void test075()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(74));
        assertEquals(137,ba.anzahl());
        assertEquals(9,ba.tiefe());
        assertFalse(ba.enthaelt(733));
        assertTrue(ba.enthaelt(561));
        assertFalse(ba.enthaelt(354));
        assertFalse(ba.enthaelt(820));
        assertFalse(ba.enthaelt(230));
        assertFalse(ba.enthaelt(40));
        assertFalse(ba.enthaelt(671));
        assertFalse(ba.enthaelt(624));
        assertFalse(ba.enthaelt(872));
        assertFalse(ba.enthaelt(375));
        assertFalse(ba.enthaelt(394));
        assertTrue(ba.enthaelt(151));
        assertTrue(ba.enthaelt(815));
        assertTrue(ba.enthaelt(951));
        assertTrue(ba.enthaelt(821));
        assertTrue(ba.enthaelt(364));
        assertTrue(ba.enthaelt(293));
        assertTrue(ba.enthaelt(58));
        assertTrue(ba.enthaelt(798));
        assertTrue(ba.enthaelt(51));
    }

    @Test
    public void test076()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(75));
        assertEquals(63,ba.anzahl());
        assertEquals(8,ba.tiefe());
        assertFalse(ba.enthaelt(259));
        assertFalse(ba.enthaelt(272));
        assertTrue(ba.enthaelt(251));
        assertFalse(ba.enthaelt(961));
        assertFalse(ba.enthaelt(214));
        assertFalse(ba.enthaelt(963));
        assertFalse(ba.enthaelt(921));
        assertTrue(ba.enthaelt(278));
        assertFalse(ba.enthaelt(521));
        assertFalse(ba.enthaelt(302));
        assertFalse(ba.enthaelt(621));
        assertFalse(ba.enthaelt(640));
        assertTrue(ba.enthaelt(655));
        assertTrue(ba.enthaelt(829));
        assertTrue(ba.enthaelt(557));
        assertTrue(ba.enthaelt(419));
        assertTrue(ba.enthaelt(566));
        assertTrue(ba.enthaelt(581));
        assertTrue(ba.enthaelt(627));
        assertTrue(ba.enthaelt(385));
    }

    @Test
    public void test077()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(76));
        assertEquals(265,ba.anzahl());
        assertEquals(11,ba.tiefe());
        assertFalse(ba.enthaelt(59));
        assertTrue(ba.enthaelt(384));
        assertFalse(ba.enthaelt(286));
        assertTrue(ba.enthaelt(195));
        assertFalse(ba.enthaelt(432));
        assertTrue(ba.enthaelt(591));
        assertFalse(ba.enthaelt(107));
        assertTrue(ba.enthaelt(867));
        assertFalse(ba.enthaelt(51));
        assertFalse(ba.enthaelt(883));
        assertFalse(ba.enthaelt(206));
        assertFalse(ba.enthaelt(981));
        assertFalse(ba.enthaelt(32));
        assertFalse(ba.enthaelt(782));
        assertTrue(ba.enthaelt(352));
        assertTrue(ba.enthaelt(804));
        assertTrue(ba.enthaelt(767));
        assertTrue(ba.enthaelt(129));
        assertTrue(ba.enthaelt(483));
        assertTrue(ba.enthaelt(372));
    }

    @Test
    public void test078()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(77));
        assertEquals(211,ba.anzahl());
        assertEquals(10,ba.tiefe());
        assertFalse(ba.enthaelt(520));
        assertFalse(ba.enthaelt(832));
        assertTrue(ba.enthaelt(98));
        assertFalse(ba.enthaelt(240));
        assertFalse(ba.enthaelt(1015));
        assertFalse(ba.enthaelt(367));
        assertFalse(ba.enthaelt(237));
        assertFalse(ba.enthaelt(873));
        assertFalse(ba.enthaelt(672));
        assertFalse(ba.enthaelt(269));
        assertTrue(ba.enthaelt(115));
        assertFalse(ba.enthaelt(493));
        assertTrue(ba.enthaelt(772));
        assertTrue(ba.enthaelt(36));
        assertTrue(ba.enthaelt(766));
        assertTrue(ba.enthaelt(329));
        assertTrue(ba.enthaelt(519));
        assertTrue(ba.enthaelt(566));
        assertTrue(ba.enthaelt(192));
        assertTrue(ba.enthaelt(114));
    }

    @Test
    public void test079()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(78));
        assertEquals(77,ba.anzahl());
        assertEquals(8,ba.tiefe());
        assertFalse(ba.enthaelt(774));
        assertFalse(ba.enthaelt(217));
        assertFalse(ba.enthaelt(274));
        assertFalse(ba.enthaelt(342));
        assertFalse(ba.enthaelt(175));
        assertTrue(ba.enthaelt(282));
        assertFalse(ba.enthaelt(191));
        assertFalse(ba.enthaelt(640));
        assertFalse(ba.enthaelt(373));
        assertFalse(ba.enthaelt(17));
        assertFalse(ba.enthaelt(950));
        assertTrue(ba.enthaelt(14));
        assertTrue(ba.enthaelt(391));
        assertTrue(ba.enthaelt(127));
        assertTrue(ba.enthaelt(402));
        assertTrue(ba.enthaelt(327));
        assertTrue(ba.enthaelt(420));
        assertTrue(ba.enthaelt(151));
        assertTrue(ba.enthaelt(313));
        assertTrue(ba.enthaelt(747));
    }

    @Test
    public void test080()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(79));
        assertEquals(301,ba.anzahl());
        assertEquals(12,ba.tiefe());
        assertFalse(ba.enthaelt(21));
        assertFalse(ba.enthaelt(562));
        assertFalse(ba.enthaelt(89));
        assertTrue(ba.enthaelt(366));
        assertFalse(ba.enthaelt(207));
        assertFalse(ba.enthaelt(825));
        assertFalse(ba.enthaelt(289));
        assertFalse(ba.enthaelt(1016));
        assertFalse(ba.enthaelt(213));
        assertFalse(ba.enthaelt(326));
        assertFalse(ba.enthaelt(407));
        assertTrue(ba.enthaelt(225));
        assertTrue(ba.enthaelt(576));
        assertTrue(ba.enthaelt(180));
        assertTrue(ba.enthaelt(504));
        assertTrue(ba.enthaelt(374));
        assertTrue(ba.enthaelt(239));
        assertTrue(ba.enthaelt(139));
        assertTrue(ba.enthaelt(327));
        assertTrue(ba.enthaelt(286));
    }

    @Test
    public void test081()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(80));
        assertEquals(577,ba.anzahl());
        assertEquals(12,ba.tiefe());
        assertFalse(ba.enthaelt(345));
        assertFalse(ba.enthaelt(461));
        assertFalse(ba.enthaelt(1007));
        assertTrue(ba.enthaelt(821));
        assertFalse(ba.enthaelt(446));
        assertTrue(ba.enthaelt(748));
        assertFalse(ba.enthaelt(400));
        assertTrue(ba.enthaelt(505));
        assertFalse(ba.enthaelt(898));
        assertTrue(ba.enthaelt(10));
        assertFalse(ba.enthaelt(72));
        assertTrue(ba.enthaelt(222));
        assertFalse(ba.enthaelt(977));
        assertTrue(ba.enthaelt(555));
        assertTrue(ba.enthaelt(870));
        assertFalse(ba.enthaelt(845));
        assertTrue(ba.enthaelt(795));
        assertTrue(ba.enthaelt(12));
        assertTrue(ba.enthaelt(233));
        assertFalse(ba.enthaelt(731));
    }

    @Test
    public void test082()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(81));
        assertEquals(99,ba.anzahl());
        assertEquals(8,ba.tiefe());
        assertFalse(ba.enthaelt(508));
        assertFalse(ba.enthaelt(542));
        assertFalse(ba.enthaelt(12));
        assertFalse(ba.enthaelt(739));
        assertFalse(ba.enthaelt(770));
        assertFalse(ba.enthaelt(1010));
        assertFalse(ba.enthaelt(1018));
        assertFalse(ba.enthaelt(15));
        assertTrue(ba.enthaelt(418));
        assertFalse(ba.enthaelt(969));
        assertFalse(ba.enthaelt(962));
        assertTrue(ba.enthaelt(191));
        assertTrue(ba.enthaelt(31));
        assertTrue(ba.enthaelt(299));
        assertTrue(ba.enthaelt(846));
        assertTrue(ba.enthaelt(768));
        assertTrue(ba.enthaelt(973));
        assertTrue(ba.enthaelt(772));
        assertTrue(ba.enthaelt(629));
        assertTrue(ba.enthaelt(844));
    }

    @Test
    public void test083()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(82));
        assertEquals(393,ba.anzahl());
        assertEquals(11,ba.tiefe());
        assertFalse(ba.enthaelt(762));
        assertFalse(ba.enthaelt(749));
        assertFalse(ba.enthaelt(560));
        assertFalse(ba.enthaelt(646));
        assertTrue(ba.enthaelt(558));
        assertFalse(ba.enthaelt(8));
        assertFalse(ba.enthaelt(683));
        assertFalse(ba.enthaelt(805));
        assertTrue(ba.enthaelt(668));
        assertTrue(ba.enthaelt(434));
        assertTrue(ba.enthaelt(483));
        assertFalse(ba.enthaelt(962));
        assertTrue(ba.enthaelt(137));
        assertTrue(ba.enthaelt(239));
        assertFalse(ba.enthaelt(747));
        assertTrue(ba.enthaelt(583));
        assertFalse(ba.enthaelt(298));
        assertTrue(ba.enthaelt(608));
        assertTrue(ba.enthaelt(914));
        assertTrue(ba.enthaelt(717));
    }

    @Test
    public void test084()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(83));
        assertEquals(203,ba.anzahl());
        assertEquals(10,ba.tiefe());
        assertFalse(ba.enthaelt(198));
        assertFalse(ba.enthaelt(805));
        assertFalse(ba.enthaelt(725));
        assertFalse(ba.enthaelt(434));
        assertTrue(ba.enthaelt(289));
        assertFalse(ba.enthaelt(330));
        assertTrue(ba.enthaelt(130));
        assertFalse(ba.enthaelt(876));
        assertFalse(ba.enthaelt(812));
        assertFalse(ba.enthaelt(989));
        assertFalse(ba.enthaelt(51));
        assertFalse(ba.enthaelt(594));
        assertTrue(ba.enthaelt(673));
        assertTrue(ba.enthaelt(988));
        assertTrue(ba.enthaelt(940));
        assertTrue(ba.enthaelt(352));
        assertTrue(ba.enthaelt(756));
        assertTrue(ba.enthaelt(435));
        assertTrue(ba.enthaelt(525));
        assertTrue(ba.enthaelt(196));
    }

    @Test
    public void test085()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(84));
        assertEquals(123,ba.anzahl());
        assertEquals(9,ba.tiefe());
        assertFalse(ba.enthaelt(332));
        assertTrue(ba.enthaelt(580));
        assertFalse(ba.enthaelt(197));
        assertTrue(ba.enthaelt(188));
        assertFalse(ba.enthaelt(175));
        assertFalse(ba.enthaelt(59));
        assertFalse(ba.enthaelt(402));
        assertFalse(ba.enthaelt(200));
        assertTrue(ba.enthaelt(265));
        assertFalse(ba.enthaelt(320));
        assertTrue(ba.enthaelt(969));
        assertFalse(ba.enthaelt(54));
        assertFalse(ba.enthaelt(190));
        assertTrue(ba.enthaelt(664));
        assertFalse(ba.enthaelt(1010));
        assertTrue(ba.enthaelt(340));
        assertTrue(ba.enthaelt(679));
        assertTrue(ba.enthaelt(909));
        assertTrue(ba.enthaelt(588));
        assertTrue(ba.enthaelt(416));
    }

    @Test
    public void test086()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(85));
        assertEquals(425,ba.anzahl());
        assertEquals(12,ba.tiefe());
        assertFalse(ba.enthaelt(406));
        assertFalse(ba.enthaelt(372));
        assertFalse(ba.enthaelt(429));
        assertFalse(ba.enthaelt(108));
        assertFalse(ba.enthaelt(158));
        assertFalse(ba.enthaelt(241));
        assertFalse(ba.enthaelt(24));
        assertFalse(ba.enthaelt(581));
        assertTrue(ba.enthaelt(258));
        assertFalse(ba.enthaelt(692));
        assertFalse(ba.enthaelt(127));
        assertTrue(ba.enthaelt(593));
        assertTrue(ba.enthaelt(212));
        assertTrue(ba.enthaelt(492));
        assertTrue(ba.enthaelt(767));
        assertTrue(ba.enthaelt(90));
        assertTrue(ba.enthaelt(457));
        assertTrue(ba.enthaelt(793));
        assertTrue(ba.enthaelt(209));
        assertTrue(ba.enthaelt(180));
    }

    @Test
    public void test087()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(86));
        assertEquals(43,ba.anzahl());
        assertEquals(9,ba.tiefe());
        assertFalse(ba.enthaelt(247));
        assertTrue(ba.enthaelt(787));
        assertFalse(ba.enthaelt(299));
        assertFalse(ba.enthaelt(117));
        assertFalse(ba.enthaelt(733));
        assertFalse(ba.enthaelt(565));
        assertFalse(ba.enthaelt(530));
        assertFalse(ba.enthaelt(363));
        assertFalse(ba.enthaelt(827));
        assertFalse(ba.enthaelt(995));
        assertFalse(ba.enthaelt(926));
        assertTrue(ba.enthaelt(294));
        assertTrue(ba.enthaelt(759));
        assertTrue(ba.enthaelt(972));
        assertTrue(ba.enthaelt(703));
        assertTrue(ba.enthaelt(739));
        assertTrue(ba.enthaelt(339));
        assertTrue(ba.enthaelt(589));
        assertTrue(ba.enthaelt(6));
        assertTrue(ba.enthaelt(450));
    }

    @Test
    public void test088()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(87));
        assertEquals(579,ba.anzahl());
        assertEquals(12,ba.tiefe());
        assertFalse(ba.enthaelt(1017));
        assertFalse(ba.enthaelt(557));
        assertTrue(ba.enthaelt(742));
        assertFalse(ba.enthaelt(95));
        assertFalse(ba.enthaelt(748));
        assertFalse(ba.enthaelt(839));
        assertFalse(ba.enthaelt(612));
        assertFalse(ba.enthaelt(131));
        assertTrue(ba.enthaelt(433));
        assertFalse(ba.enthaelt(64));
        assertFalse(ba.enthaelt(739));
        assertTrue(ba.enthaelt(831));
        assertTrue(ba.enthaelt(743));
        assertTrue(ba.enthaelt(368));
        assertFalse(ba.enthaelt(674));
        assertTrue(ba.enthaelt(889));
        assertTrue(ba.enthaelt(81));
        assertTrue(ba.enthaelt(435));
        assertTrue(ba.enthaelt(13));
        assertTrue(ba.enthaelt(350));
    }

    @Test
    public void test089()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(88));
        assertEquals(229,ba.anzahl());
        assertEquals(10,ba.tiefe());
        assertTrue(ba.enthaelt(37));
        assertFalse(ba.enthaelt(73));
        assertFalse(ba.enthaelt(642));
        assertTrue(ba.enthaelt(83));
        assertFalse(ba.enthaelt(38));
        assertFalse(ba.enthaelt(859));
        assertTrue(ba.enthaelt(324));
        assertFalse(ba.enthaelt(475));
        assertTrue(ba.enthaelt(793));
        assertFalse(ba.enthaelt(472));
        assertFalse(ba.enthaelt(81));
        assertFalse(ba.enthaelt(480));
        assertFalse(ba.enthaelt(954));
        assertFalse(ba.enthaelt(466));
        assertTrue(ba.enthaelt(984));
        assertTrue(ba.enthaelt(540));
        assertTrue(ba.enthaelt(316));
        assertTrue(ba.enthaelt(721));
        assertTrue(ba.enthaelt(927));
        assertTrue(ba.enthaelt(517));
    }

    @Test
    public void test090()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(89));
        assertEquals(159,ba.anzahl());
        assertEquals(9,ba.tiefe());
        assertTrue(ba.enthaelt(686));
        assertFalse(ba.enthaelt(84));
        assertFalse(ba.enthaelt(210));
        assertFalse(ba.enthaelt(541));
        assertTrue(ba.enthaelt(775));
        assertFalse(ba.enthaelt(429));
        assertFalse(ba.enthaelt(781));
        assertFalse(ba.enthaelt(133));
        assertTrue(ba.enthaelt(578));
        assertFalse(ba.enthaelt(103));
        assertFalse(ba.enthaelt(750));
        assertFalse(ba.enthaelt(295));
        assertFalse(ba.enthaelt(19));
        assertTrue(ba.enthaelt(278));
        assertTrue(ba.enthaelt(189));
        assertTrue(ba.enthaelt(826));
        assertTrue(ba.enthaelt(511));
        assertTrue(ba.enthaelt(999));
        assertTrue(ba.enthaelt(731));
        assertTrue(ba.enthaelt(39));
    }

    @Test
    public void test091()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(90));
        assertEquals(329,ba.anzahl());
        assertEquals(11,ba.tiefe());
        assertFalse(ba.enthaelt(161));
        assertFalse(ba.enthaelt(698));
        assertFalse(ba.enthaelt(777));
        assertFalse(ba.enthaelt(204));
        assertFalse(ba.enthaelt(373));
        assertFalse(ba.enthaelt(467));
        assertTrue(ba.enthaelt(42));
        assertFalse(ba.enthaelt(897));
        assertTrue(ba.enthaelt(533));
        assertFalse(ba.enthaelt(864));
        assertTrue(ba.enthaelt(538));
        assertFalse(ba.enthaelt(142));
        assertFalse(ba.enthaelt(667));
        assertTrue(ba.enthaelt(783));
        assertTrue(ba.enthaelt(422));
        assertTrue(ba.enthaelt(48));
        assertTrue(ba.enthaelt(81));
        assertTrue(ba.enthaelt(478));
        assertTrue(ba.enthaelt(99));
        assertTrue(ba.enthaelt(834));
    }

    @Test
    public void test092()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(91));
        assertEquals(87,ba.anzahl());
        assertEquals(8,ba.tiefe());
        assertFalse(ba.enthaelt(77));
        assertTrue(ba.enthaelt(369));
        assertFalse(ba.enthaelt(596));
        assertFalse(ba.enthaelt(777));
        assertFalse(ba.enthaelt(899));
        assertFalse(ba.enthaelt(901));
        assertFalse(ba.enthaelt(985));
        assertFalse(ba.enthaelt(891));
        assertFalse(ba.enthaelt(785));
        assertFalse(ba.enthaelt(700));
        assertFalse(ba.enthaelt(98));
        assertTrue(ba.enthaelt(996));
        assertTrue(ba.enthaelt(356));
        assertTrue(ba.enthaelt(688));
        assertTrue(ba.enthaelt(746));
        assertTrue(ba.enthaelt(554));
        assertTrue(ba.enthaelt(92));
        assertTrue(ba.enthaelt(238));
        assertTrue(ba.enthaelt(779));
        assertTrue(ba.enthaelt(105));
    }

    @Test
    public void test093()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(92));
        assertEquals(99,ba.anzahl());
        assertEquals(8,ba.tiefe());
        assertFalse(ba.enthaelt(185));
        assertFalse(ba.enthaelt(447));
        assertFalse(ba.enthaelt(401));
        assertFalse(ba.enthaelt(86));
        assertFalse(ba.enthaelt(744));
        assertFalse(ba.enthaelt(535));
        assertFalse(ba.enthaelt(886));
        assertFalse(ba.enthaelt(775));
        assertFalse(ba.enthaelt(810));
        assertFalse(ba.enthaelt(988));
        assertTrue(ba.enthaelt(67));
        assertTrue(ba.enthaelt(678));
        assertTrue(ba.enthaelt(50));
        assertTrue(ba.enthaelt(144));
        assertTrue(ba.enthaelt(959));
        assertTrue(ba.enthaelt(448));
        assertTrue(ba.enthaelt(636));
        assertTrue(ba.enthaelt(633));
        assertTrue(ba.enthaelt(296));
        assertTrue(ba.enthaelt(947));
    }

    @Test
    public void test094()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(93));
        assertEquals(137,ba.anzahl());
        assertEquals(9,ba.tiefe());
        assertFalse(ba.enthaelt(965));
        assertFalse(ba.enthaelt(826));
        assertFalse(ba.enthaelt(551));
        assertFalse(ba.enthaelt(651));
        assertFalse(ba.enthaelt(740));
        assertFalse(ba.enthaelt(993));
        assertFalse(ba.enthaelt(11));
        assertFalse(ba.enthaelt(497));
        assertFalse(ba.enthaelt(700));
        assertFalse(ba.enthaelt(177));
        assertTrue(ba.enthaelt(125));
        assertTrue(ba.enthaelt(816));
        assertTrue(ba.enthaelt(26));
        assertTrue(ba.enthaelt(80));
        assertTrue(ba.enthaelt(824));
        assertTrue(ba.enthaelt(381));
        assertTrue(ba.enthaelt(687));
        assertTrue(ba.enthaelt(37));
        assertTrue(ba.enthaelt(943));
        assertTrue(ba.enthaelt(363));
    }

    @Test
    public void test095()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(94));
        assertEquals(357,ba.anzahl());
        assertEquals(12,ba.tiefe());
        assertFalse(ba.enthaelt(229));
        assertFalse(ba.enthaelt(313));
        assertTrue(ba.enthaelt(769));
        assertFalse(ba.enthaelt(74));
        assertFalse(ba.enthaelt(936));
        assertFalse(ba.enthaelt(373));
        assertFalse(ba.enthaelt(473));
        assertFalse(ba.enthaelt(694));
        assertFalse(ba.enthaelt(663));
        assertTrue(ba.enthaelt(391));
        assertTrue(ba.enthaelt(225));
        assertTrue(ba.enthaelt(783));
        assertFalse(ba.enthaelt(224));
        assertFalse(ba.enthaelt(531));
        assertTrue(ba.enthaelt(625));
        assertTrue(ba.enthaelt(186));
        assertTrue(ba.enthaelt(442));
        assertTrue(ba.enthaelt(374));
        assertTrue(ba.enthaelt(597));
        assertTrue(ba.enthaelt(999));
    }

    @Test
    public void test096()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(95));
        assertEquals(79,ba.anzahl());
        assertEquals(10,ba.tiefe());
        assertFalse(ba.enthaelt(630));
        assertFalse(ba.enthaelt(193));
        assertFalse(ba.enthaelt(100));
        assertFalse(ba.enthaelt(73));
        assertFalse(ba.enthaelt(926));
        assertFalse(ba.enthaelt(417));
        assertFalse(ba.enthaelt(214));
        assertTrue(ba.enthaelt(63));
        assertFalse(ba.enthaelt(329));
        assertFalse(ba.enthaelt(246));
        assertFalse(ba.enthaelt(934));
        assertTrue(ba.enthaelt(198));
        assertTrue(ba.enthaelt(256));
        assertTrue(ba.enthaelt(655));
        assertTrue(ba.enthaelt(441));
        assertTrue(ba.enthaelt(927));
        assertTrue(ba.enthaelt(74));
        assertTrue(ba.enthaelt(576));
        assertTrue(ba.enthaelt(702));
        assertTrue(ba.enthaelt(333));
    }

    @Test
    public void test097()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(96));
        assertEquals(141,ba.anzahl());
        assertEquals(9,ba.tiefe());
        assertFalse(ba.enthaelt(98));
        assertFalse(ba.enthaelt(259));
        assertFalse(ba.enthaelt(169));
        assertFalse(ba.enthaelt(917));
        assertFalse(ba.enthaelt(103));
        assertFalse(ba.enthaelt(15));
        assertFalse(ba.enthaelt(931));
        assertFalse(ba.enthaelt(265));
        assertFalse(ba.enthaelt(226));
        assertTrue(ba.enthaelt(639));
        assertFalse(ba.enthaelt(775));
        assertTrue(ba.enthaelt(150));
        assertTrue(ba.enthaelt(183));
        assertTrue(ba.enthaelt(281));
        assertTrue(ba.enthaelt(510));
        assertTrue(ba.enthaelt(428));
        assertTrue(ba.enthaelt(631));
        assertTrue(ba.enthaelt(221));
        assertTrue(ba.enthaelt(533));
        assertTrue(ba.enthaelt(12));
    }

    @Test
    public void test098()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(97));
        assertEquals(139,ba.anzahl());
        assertEquals(9,ba.tiefe());
        assertFalse(ba.enthaelt(889));
        assertFalse(ba.enthaelt(987));
        assertFalse(ba.enthaelt(441));
        assertFalse(ba.enthaelt(33));
        assertFalse(ba.enthaelt(485));
        assertFalse(ba.enthaelt(482));
        assertFalse(ba.enthaelt(946));
        assertFalse(ba.enthaelt(299));
        assertFalse(ba.enthaelt(227));
        assertFalse(ba.enthaelt(147));
        assertTrue(ba.enthaelt(763));
        assertTrue(ba.enthaelt(63));
        assertTrue(ba.enthaelt(169));
        assertTrue(ba.enthaelt(57));
        assertTrue(ba.enthaelt(100));
        assertTrue(ba.enthaelt(263));
        assertTrue(ba.enthaelt(401));
        assertTrue(ba.enthaelt(301));
        assertTrue(ba.enthaelt(214));
        assertTrue(ba.enthaelt(30));
    }

    @Test
    public void test099()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(98));
        assertEquals(169,ba.anzahl());
        assertEquals(10,ba.tiefe());
        assertFalse(ba.enthaelt(896));
        assertFalse(ba.enthaelt(714));
        assertTrue(ba.enthaelt(308));
        assertTrue(ba.enthaelt(597));
        assertFalse(ba.enthaelt(354));
        assertFalse(ba.enthaelt(760));
        assertTrue(ba.enthaelt(610));
        assertFalse(ba.enthaelt(411));
        assertFalse(ba.enthaelt(259));
        assertFalse(ba.enthaelt(127));
        assertFalse(ba.enthaelt(541));
        assertFalse(ba.enthaelt(269));
        assertFalse(ba.enthaelt(379));
        assertTrue(ba.enthaelt(322));
        assertTrue(ba.enthaelt(670));
        assertTrue(ba.enthaelt(568));
        assertTrue(ba.enthaelt(156));
        assertTrue(ba.enthaelt(882));
        assertTrue(ba.enthaelt(952));
        assertTrue(ba.enthaelt(102));
    }

    @Test
    public void test100()
    {
        Baumalgorithmen ba = new Baumalgorithmen(Testbaeume.getTestBaum(99));
        assertEquals(119,ba.anzahl());
        assertEquals(9,ba.tiefe());
        assertFalse(ba.enthaelt(334));
        assertFalse(ba.enthaelt(0));
        assertFalse(ba.enthaelt(307));
        assertFalse(ba.enthaelt(572));
        assertFalse(ba.enthaelt(273));
        assertFalse(ba.enthaelt(644));
        assertFalse(ba.enthaelt(671));
        assertFalse(ba.enthaelt(976));
        assertFalse(ba.enthaelt(1007));
        assertFalse(ba.enthaelt(697));
        assertTrue(ba.enthaelt(968));
        assertTrue(ba.enthaelt(33));
        assertTrue(ba.enthaelt(411));
        assertTrue(ba.enthaelt(780));
        assertTrue(ba.enthaelt(387));
        assertTrue(ba.enthaelt(263));
        assertTrue(ba.enthaelt(396));
        assertTrue(ba.enthaelt(198));
        assertTrue(ba.enthaelt(6));
        assertTrue(ba.enthaelt(28));
    }
}
